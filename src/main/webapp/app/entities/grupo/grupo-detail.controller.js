(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('GrupoDetailController', GrupoDetailController);

    GrupoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Grupo', 'Jardin', 'NivelJardin'];

    function GrupoDetailController($scope, $rootScope, $stateParams, previousState, entity, Grupo, Jardin, NivelJardin) {
        var vm = this;

        vm.grupo = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:grupoUpdate', function(event, result) {
            vm.grupo = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
