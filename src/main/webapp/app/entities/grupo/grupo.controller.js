(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('GrupoController', GrupoController);

    GrupoController.$inject = ['$scope', '$state', 'Grupo'];

    function GrupoController ($scope, $state, Grupo) {
        var vm = this;
        
        vm.grupos = [];

        loadAll();

        function loadAll() {
            Grupo.query(function(result) {
                vm.grupos = result;
            });
        }
    }
})();
