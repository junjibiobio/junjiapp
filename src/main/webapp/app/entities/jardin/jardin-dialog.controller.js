(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('JardinDialogController', JardinDialogController);

    JardinDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Jardin', 'DireccionRegional', 'Telefono', 'NivelJardin', 'ContratoSuministro'];

    function JardinDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Jardin, DireccionRegional, Telefono, NivelJardin, ContratoSuministro) {
        var vm = this;

        vm.jardin = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.direccionregionals = DireccionRegional.query();
        vm.telefonos = Telefono.query();
        vm.niveljardins = NivelJardin.query();
        vm.contratosuministros = ContratoSuministro.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.jardin.id !== null) {
                Jardin.update(vm.jardin, onSaveSuccess, onSaveError);
            } else {
                Jardin.save(vm.jardin, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:jardinUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaCreacionJardin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
