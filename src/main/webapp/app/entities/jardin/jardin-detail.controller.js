(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('JardinDetailController', JardinDetailController);

    JardinDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Jardin', 'DireccionRegional', 'Telefono', 'NivelJardin', 'ContratoSuministro'];

    function JardinDetailController($scope, $rootScope, $stateParams, previousState, entity, Jardin, DireccionRegional, Telefono, NivelJardin, ContratoSuministro) {
        var vm = this;

        vm.jardin = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:jardinUpdate', function(event, result) {
            vm.jardin = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
