(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('Jardin', Jardin)
        .factory('NuevosJardines', NuevosJardines)
        .factory('PreJardines', PreJardines)
        .factory('Jardines', Jardines);

    Jardin.$inject = ['$resource', 'DateUtils'];
    Jardines.$inject = ['$resource', 'DateUtils'];
    NuevosJardines.$inject = ['$resource', 'DateUtils'];
    PreJardines.$inject = ['$resource', 'DateUtils'];

    function Jardin ($resource, DateUtils) {
        var resourceUrl =  'api/jardins/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fechaCreacionJardin = DateUtils.convertLocalDateFromServer(data.fechaCreacionJardin);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaCreacionJardin = DateUtils.convertLocalDateToServer(data.fechaCreacionJardin);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaCreacionJardin = DateUtils.convertLocalDateToServer(data.fechaCreacionJardin);
                    return angular.toJson(data);
                }
            }
        });
    }
    function Jardines ($resource, DateUtils) {
        var resourceUrl =  'api/jardines/:valor';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
    function NuevosJardines ($resource, DateUtils) {
        var resourceUrl =  'api/jardines/nuevos';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
    function PreJardines ($resource, DateUtils) {
        var resourceUrl =  'api/jardines/preinauguracion';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
