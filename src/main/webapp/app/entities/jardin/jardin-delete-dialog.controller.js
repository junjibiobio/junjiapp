(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('JardinDeleteController',JardinDeleteController);

    JardinDeleteController.$inject = ['$uibModalInstance', 'entity', 'Jardin'];

    function JardinDeleteController($uibModalInstance, entity, Jardin) {
        var vm = this;

        vm.jardin = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Jardin.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
