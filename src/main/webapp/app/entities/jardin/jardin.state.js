(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('jardin', {
            parent: 'entity',
            url: '/jardin?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Jardins'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/jardin/jardins.html',
                    controller: 'JardinController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('jardin-detail', {
            parent: 'entity',
            url: '/jardin/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Jardin'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/jardin/jardin-detail.html',
                    controller: 'JardinDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Jardin', function($stateParams, Jardin) {
                    return Jardin.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'jardin',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('jardin-detail.edit', {
            parent: 'jardin-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jardin/jardin-dialog.html',
                    controller: 'JardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Jardin', function(Jardin) {
                            return Jardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('jardin.new', {
            parent: 'jardin',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jardin/jardin-dialog.html',
                    controller: 'JardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                codigoJardin: null,
                                nombreJardin: null,
                                estadoJardin: null,
                                fechaCreacionJardin: null,
                                programaJardin: null,
                                modalidadJardin: null,
                                ruralidadJardin: null,
                                direccionJardin: null,
                                provinciaJardin: null,
                                comunaJardin: null,
                                latitudJardin: null,
                                longitudJardin: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('jardin', null, { reload: true });
                }, function() {
                    $state.go('jardin');
                });
            }]
        })
        .state('jardin.edit', {
            parent: 'jardin',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jardin/jardin-dialog.html',
                    controller: 'JardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Jardin', function(Jardin) {
                            return Jardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('jardin', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('jardin.delete', {
            parent: 'jardin',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jardin/jardin-delete-dialog.html',
                    controller: 'JardinDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Jardin', function(Jardin) {
                            return Jardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('jardin', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
