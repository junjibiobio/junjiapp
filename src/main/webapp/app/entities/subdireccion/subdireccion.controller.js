(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('SubdireccionController', SubdireccionController);

    SubdireccionController.$inject = ['$scope', '$state', 'Subdireccion'];

    function SubdireccionController ($scope, $state, Subdireccion) {
        var vm = this;
        vm.subdireccions = [];
        vm.loadAll = function() {
            Subdireccion.query(function(result) {
                vm.subdireccions = result;
            });
        };

        vm.loadAll();
        
    }
})();
