(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('SubdireccionDeleteController',SubdireccionDeleteController);

    SubdireccionDeleteController.$inject = ['$uibModalInstance', 'entity', 'Subdireccion'];

    function SubdireccionDeleteController($uibModalInstance, entity, Subdireccion) {
        var vm = this;
        vm.subdireccion = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Subdireccion.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
