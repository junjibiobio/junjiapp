(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('Subdireccion', Subdireccion);

    Subdireccion.$inject = ['$resource'];

    function Subdireccion ($resource) {
        var resourceUrl =  'api/subdireccions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
