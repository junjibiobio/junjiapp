(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('SubdireccionDialogController', SubdireccionDialogController);

    SubdireccionDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Subdireccion', 'DireccionRegional', 'Unidad'];

    function SubdireccionDialogController ($scope, $stateParams, $uibModalInstance, entity, Subdireccion, DireccionRegional, Unidad) {
        var vm = this;
        vm.subdireccion = entity;
        vm.direccionregionals = DireccionRegional.query();
        vm.unidads = Unidad.query();
        vm.load = function(id) {
            Subdireccion.get({id : id}, function(result) {
                vm.subdireccion = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('junjiwebApp:subdireccionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.subdireccion.id !== null) {
                Subdireccion.update(vm.subdireccion, onSaveSuccess, onSaveError);
            } else {
                Subdireccion.save(vm.subdireccion, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
