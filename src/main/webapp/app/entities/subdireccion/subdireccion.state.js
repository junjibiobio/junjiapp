(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('subdireccion', {
            parent: 'entity',
            url: '/subdireccion',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Subdireccions'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/subdireccion/subdireccions.html',
                    controller: 'SubdireccionController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('subdireccion-detail', {
            parent: 'entity',
            url: '/subdireccion/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Subdireccion'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/subdireccion/subdireccion-detail.html',
                    controller: 'SubdireccionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Subdireccion', function($stateParams, Subdireccion) {
                    return Subdireccion.get({id : $stateParams.id});
                }]
            }
        })
        .state('subdireccion.new', {
            parent: 'subdireccion',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/subdireccion/subdireccion-dialog.html',
                    controller: 'SubdireccionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nombreSubdireccion: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('subdireccion', null, { reload: true });
                }, function() {
                    $state.go('subdireccion');
                });
            }]
        })
        .state('subdireccion.edit', {
            parent: 'subdireccion',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/subdireccion/subdireccion-dialog.html',
                    controller: 'SubdireccionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Subdireccion', function(Subdireccion) {
                            return Subdireccion.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('subdireccion', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('subdireccion.delete', {
            parent: 'subdireccion',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/subdireccion/subdireccion-delete-dialog.html',
                    controller: 'SubdireccionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Subdireccion', function(Subdireccion) {
                            return Subdireccion.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('subdireccion', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
