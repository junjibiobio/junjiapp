(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('SubdireccionDetailController', SubdireccionDetailController);

    SubdireccionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Subdireccion', 'DireccionRegional', 'Unidad'];

    function SubdireccionDetailController($scope, $rootScope, $stateParams, entity, Subdireccion, DireccionRegional, Unidad) {
        var vm = this;
        vm.subdireccion = entity;
        vm.load = function (id) {
            Subdireccion.get({id: id}, function(result) {
                vm.subdireccion = result;
            });
        };
        var unsubscribe = $rootScope.$on('junjiwebApp:subdireccionUpdate', function(event, result) {
            vm.subdireccion = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
