(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('NivelJardinDialogController', NivelJardinDialogController);

    NivelJardinDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NivelJardin', 'Jardin'];

    function NivelJardinDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NivelJardin, Jardin) {
        var vm = this;

        vm.nivelJardin = entity;
        vm.clear = clear;
        vm.save = save;
        vm.jardins = Jardin.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.nivelJardin.id !== null) {
                NivelJardin.update(vm.nivelJardin, onSaveSuccess, onSaveError);
            } else {
                NivelJardin.save(vm.nivelJardin, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:nivelJardinUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
