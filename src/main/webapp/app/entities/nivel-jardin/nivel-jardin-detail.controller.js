(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('NivelJardinDetailController', NivelJardinDetailController);

    NivelJardinDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NivelJardin', 'Jardin'];

    function NivelJardinDetailController($scope, $rootScope, $stateParams, previousState, entity, NivelJardin, Jardin) {
        var vm = this;

        vm.nivelJardin = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:nivelJardinUpdate', function(event, result) {
            vm.nivelJardin = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
