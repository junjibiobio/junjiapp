(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('NivelJardin', NivelJardin);

    NivelJardin.$inject = ['$resource'];

    function NivelJardin ($resource) {
        var resourceUrl =  'api/nivel-jardins/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
