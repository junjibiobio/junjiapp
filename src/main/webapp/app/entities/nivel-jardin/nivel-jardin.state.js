(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('nivel-jardin', {
            parent: 'entity',
            url: '/nivel-jardin',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'NivelJardins'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardins.html',
                    controller: 'NivelJardinController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('nivel-jardin-detail', {
            parent: 'entity',
            url: '/nivel-jardin/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'NivelJardin'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardin-detail.html',
                    controller: 'NivelJardinDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NivelJardin', function($stateParams, NivelJardin) {
                    return NivelJardin.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'nivel-jardin',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('nivel-jardin-detail.edit', {
            parent: 'nivel-jardin-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardin-dialog.html',
                    controller: 'NivelJardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NivelJardin', function(NivelJardin) {
                            return NivelJardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('nivel-jardin.new', {
            parent: 'nivel-jardin',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardin-dialog.html',
                    controller: 'NivelJardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nombreNivel: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('nivel-jardin', null, { reload: true });
                }, function() {
                    $state.go('nivel-jardin');
                });
            }]
        })
        .state('nivel-jardin.edit', {
            parent: 'nivel-jardin',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardin-dialog.html',
                    controller: 'NivelJardinDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NivelJardin', function(NivelJardin) {
                            return NivelJardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('nivel-jardin', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('nivel-jardin.delete', {
            parent: 'nivel-jardin',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/nivel-jardin/nivel-jardin-delete-dialog.html',
                    controller: 'NivelJardinDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NivelJardin', function(NivelJardin) {
                            return NivelJardin.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('nivel-jardin', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
