(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('NivelJardinDeleteController',NivelJardinDeleteController);

    NivelJardinDeleteController.$inject = ['$uibModalInstance', 'entity', 'NivelJardin'];

    function NivelJardinDeleteController($uibModalInstance, entity, NivelJardin) {
        var vm = this;

        vm.nivelJardin = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NivelJardin.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
