(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('NivelJardinController', NivelJardinController);

    NivelJardinController.$inject = ['$scope', '$state', 'NivelJardin'];

    function NivelJardinController ($scope, $state, NivelJardin) {
        var vm = this;
        
        vm.nivelJardins = [];

        loadAll();

        function loadAll() {
            NivelJardin.query(function(result) {
                vm.nivelJardins = result;
            });
        }
    }
})();
