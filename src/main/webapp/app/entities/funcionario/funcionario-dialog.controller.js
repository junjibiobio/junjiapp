(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('FuncionarioDialogController', FuncionarioDialogController);

    FuncionarioDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Funcionario', 'Unidad', 'Telefono', 'ProcesoCS'];

    function FuncionarioDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Funcionario, Unidad, Telefono, ProcesoCS) {
        var vm = this;

        vm.funcionario = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.unidads = Unidad.query();
        vm.telefonos = Telefono.query();
        vm.procesocs = ProcesoCS.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.funcionario.id !== null) {
                Funcionario.update(vm.funcionario, onSaveSuccess, onSaveError);
            } else {
                Funcionario.save(vm.funcionario, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:funcionarioUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaContratoFuncionario = false;
        vm.datePickerOpenStatus.fechaNacimientoFuncionario = false;

        vm.setImagenFuncionario = function ($file, funcionario) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        funcionario.imagenFuncionario = base64Data;
                        funcionario.imagenFuncionarioContentType = $file.type;
                    });
                });
            }
        };

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
