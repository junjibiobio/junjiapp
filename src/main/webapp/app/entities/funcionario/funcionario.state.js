(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('funcionario', {
            parent: 'entity',
            url: '/funcionario?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Funcionarios'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/funcionario/funcionarios.html',
                    controller: 'FuncionarioController',
                    controllerAs: 'vm'
                }
            },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }]
                }

        })
        .state('funcionario-detail', {
            parent: 'entity',
            url: '/funcionario/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Funcionario'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/funcionario/funcionario-detail.html',
                    controller: 'FuncionarioDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Funcionario', function($stateParams, Funcionario) {
                    return Funcionario.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'funcionario',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('funcionario-detail.edit', {
            parent: 'funcionario-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionario/funcionario-dialog.html',
                    controller: 'FuncionarioDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Funcionario', function(Funcionario) {
                            return Funcionario.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('funcionario.new', {
            parent: 'funcionario',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionario/funcionario-dialog.html',
                    controller: 'FuncionarioDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                rutFuncionario: null,
                                nombresFuncionario: null,
                                apellidopFuncionario: null,
                                apellidomFuncionario: null,
                                correoFuncionario: null,
                                fechaContratoFuncionario: null,
                                fechaNacimientoFuncionario: null,
                                imagenFuncionario: null,
                                imagenFuncionarioContentType: null,
                                cargoFuncionario: null,
                                estamentoFuncionario: null,
                                calidadJuridicaFuncionario: null,
                                direccionFuncionario: null,
                                codigoFuncionario: null,
                                gradoFuncionario: null,
                                estadoFuncionario: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('funcionario', null, { reload: true });
                }, function() {
                    $state.go('funcionario');
                });
            }]
        })
        .state('funcionario.edit', {
            parent: 'funcionario',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionario/funcionario-dialog.html',
                    controller: 'FuncionarioDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Funcionario', function(Funcionario) {
                            return Funcionario.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('funcionario', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('funcionario.delete', {
            parent: 'funcionario',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionario/funcionario-delete-dialog.html',
                    controller: 'FuncionarioDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Funcionario', function(Funcionario) {
                            return Funcionario.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('funcionario', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
