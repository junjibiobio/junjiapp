(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('FuncionarioDeleteController',FuncionarioDeleteController);

    FuncionarioDeleteController.$inject = ['$uibModalInstance', 'entity', 'Funcionario'];

    function FuncionarioDeleteController($uibModalInstance, entity, Funcionario) {
        var vm = this;

        vm.funcionario = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Funcionario.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
