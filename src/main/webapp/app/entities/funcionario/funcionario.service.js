(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('Funcionario', Funcionario);

    Funcionario.$inject = ['$resource', 'DateUtils'];


    function Funcionario ($resource, DateUtils) {
        var resourceUrl =  'api/funcionarios/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fechaContratoFuncionario = DateUtils.convertLocalDateFromServer(data.fechaContratoFuncionario);
                        data.fechaNacimientoFuncionario = DateUtils.convertLocalDateFromServer(data.fechaNacimientoFuncionario);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaContratoFuncionario = DateUtils.convertLocalDateToServer(data.fechaContratoFuncionario);
                    data.fechaNacimientoFuncionario = DateUtils.convertLocalDateToServer(data.fechaNacimientoFuncionario);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaContratoFuncionario = DateUtils.convertLocalDateToServer(data.fechaContratoFuncionario);
                    data.fechaNacimientoFuncionario = DateUtils.convertLocalDateToServer(data.fechaNacimientoFuncionario);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
