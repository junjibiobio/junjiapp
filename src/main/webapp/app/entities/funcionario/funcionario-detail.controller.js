(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('FuncionarioDetailController', FuncionarioDetailController);

    FuncionarioDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Funcionario', 'Unidad', 'Telefono', 'ContratoSuministro', 'ProcesoCS'];

    function FuncionarioDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Funcionario, Unidad, Telefono, ContratoSuministro, ProcesoCS) {
        var vm = this;

        vm.funcionario = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('junjiwebApp:funcionarioUpdate', function(event, result) {
            vm.funcionario = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
