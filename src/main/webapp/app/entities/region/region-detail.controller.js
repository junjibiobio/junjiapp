(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('RegionDetailController', RegionDetailController);

    RegionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Region', 'DireccionRegional'];

    function RegionDetailController($scope, $rootScope, $stateParams, entity, Region, DireccionRegional) {
        var vm = this;
        vm.region = entity;
        vm.load = function (id) {
            Region.get({id: id}, function(result) {
                vm.region = result;
            });
        };
        var unsubscribe = $rootScope.$on('junjiwebApp:regionUpdate', function(event, result) {
            vm.region = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
