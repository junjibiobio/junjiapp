(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('RegionController', RegionController);

    RegionController.$inject = ['$scope', '$state', 'Region'];

    function RegionController ($scope, $state, Region) {
        var vm = this;
        vm.regions = [];
        vm.loadAll = function() {
            Region.query(function(result) {
                vm.regions = result;
            });
        };

        vm.loadAll();
        
    }
})();
