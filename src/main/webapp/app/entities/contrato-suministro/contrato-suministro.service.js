(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('ContratoSuministro', ContratoSuministro);

    ContratoSuministro.$inject = ['$resource', 'DateUtils'];

    function ContratoSuministro ($resource, DateUtils) {
        var resourceUrl =  'api/contrato-suministros/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fechaCreacionCS = DateUtils.convertDateTimeFromServer(data.fechaCreacionCS);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
