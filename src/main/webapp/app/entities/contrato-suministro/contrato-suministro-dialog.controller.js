(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ContratoSuministroDialogController', ContratoSuministroDialogController);

    ContratoSuministroDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContratoSuministro', 'Jardin', 'ProcesoCS'];

    function ContratoSuministroDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ContratoSuministro, Jardin, ProcesoCS) {
        var vm = this;

        vm.contratoSuministro = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.jardins = Jardin.query();
        vm.procesocs = ProcesoCS.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contratoSuministro.id !== null) {
                ContratoSuministro.update(vm.contratoSuministro, onSaveSuccess, onSaveError);
            } else {
                ContratoSuministro.save(vm.contratoSuministro, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:contratoSuministroUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaCreacionCS = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
