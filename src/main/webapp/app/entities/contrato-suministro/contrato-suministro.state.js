(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('contrato-suministro', {
            parent: 'entity',
            url: '/contrato-suministro',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ContratoSuministros'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministros.html',
                    controller: 'ContratoSuministroController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('contrato-suministro-detail', {
            parent: 'entity',
            url: '/contrato-suministro/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ContratoSuministro'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministro-detail.html',
                    controller: 'ContratoSuministroDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ContratoSuministro', function($stateParams, ContratoSuministro) {
                    return ContratoSuministro.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'contrato-suministro',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('contrato-suministro-detail.edit', {
            parent: 'contrato-suministro-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministro-dialog.html',
                    controller: 'ContratoSuministroDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ContratoSuministro', function(ContratoSuministro) {
                            return ContratoSuministro.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('contrato-suministro.new', {
            parent: 'contrato-suministro',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministro-dialog.html',
                    controller: 'ContratoSuministroDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                fechaCreacionCS: null,
                                tituloCS: null,
                                valorCS: null,
                                estadoCS: null,
                                tipologiaCS: null,
                                porcentajeAvanceCS: null,
                                creadoPor: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('contrato-suministro', null, { reload: true });
                }, function() {
                    $state.go('contrato-suministro');
                });
            }]
        })
        .state('contrato-suministro.edit', {
            parent: 'contrato-suministro',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministro-dialog.html',
                    controller: 'ContratoSuministroDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ContratoSuministro', function(ContratoSuministro) {
                            return ContratoSuministro.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contrato-suministro', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('contrato-suministro.delete', {
            parent: 'contrato-suministro',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrato-suministro/contrato-suministro-delete-dialog.html',
                    controller: 'ContratoSuministroDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ContratoSuministro', function(ContratoSuministro) {
                            return ContratoSuministro.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contrato-suministro', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
