(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ContratoSuministroDetailController', ContratoSuministroDetailController);

    ContratoSuministroDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ContratoSuministro', 'Jardin', 'ProcesoCS'];

    function ContratoSuministroDetailController($scope, $rootScope, $stateParams, previousState, entity, ContratoSuministro, Jardin, ProcesoCS) {
        var vm = this;

        vm.contratoSuministro = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:contratoSuministroUpdate', function(event, result) {
            vm.contratoSuministro = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
