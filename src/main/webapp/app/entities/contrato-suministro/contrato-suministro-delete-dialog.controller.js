(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ContratoSuministroDeleteController',ContratoSuministroDeleteController);

    ContratoSuministroDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContratoSuministro'];

    function ContratoSuministroDeleteController($uibModalInstance, entity, ContratoSuministro) {
        var vm = this;

        vm.contratoSuministro = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ContratoSuministro.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
