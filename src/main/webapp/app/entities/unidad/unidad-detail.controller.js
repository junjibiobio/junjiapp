(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('UnidadDetailController', UnidadDetailController);

    UnidadDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Unidad', 'Subdireccion', 'Funcionario'];

    function UnidadDetailController($scope, $rootScope, $stateParams, entity, Unidad, Subdireccion, Funcionario) {
        var vm = this;
        vm.unidad = entity;
        vm.load = function (id) {
            Unidad.get({id: id}, function(result) {
                vm.unidad = result;
            });
        };
        var unsubscribe = $rootScope.$on('junjiwebApp:unidadUpdate', function(event, result) {
            vm.unidad = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
