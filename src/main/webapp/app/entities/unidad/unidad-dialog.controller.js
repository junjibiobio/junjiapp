(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('UnidadDialogController', UnidadDialogController);

    UnidadDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Unidad', 'Subdireccion', 'Funcionario'];

    function UnidadDialogController ($scope, $stateParams, $uibModalInstance, entity, Unidad, Subdireccion, Funcionario) {
        var vm = this;
        vm.unidad = entity;
        vm.subdireccions = Subdireccion.query();
        vm.funcionarios = Funcionario.query();
        vm.load = function(id) {
            Unidad.get({id : id}, function(result) {
                vm.unidad = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('junjiwebApp:unidadUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.unidad.id !== null) {
                Unidad.update(vm.unidad, onSaveSuccess, onSaveError);
            } else {
                Unidad.save(vm.unidad, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
