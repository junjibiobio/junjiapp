(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('UnidadController', UnidadController);

    UnidadController.$inject = ['$scope', '$state', 'Unidad'];

    function UnidadController ($scope, $state, Unidad) {
        var vm = this;
        vm.unidads = [];
        vm.loadAll = function() {
            Unidad.query(function(result) {
                vm.unidads = result;
            });
        };

        vm.loadAll();
        
    }
})();
