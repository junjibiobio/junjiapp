(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('unidad', {
            parent: 'entity',
            url: '/unidad',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Unidads'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/unidad/unidads.html',
                    controller: 'UnidadController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('unidad-detail', {
            parent: 'entity',
            url: '/unidad/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Unidad'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/unidad/unidad-detail.html',
                    controller: 'UnidadDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Unidad', function($stateParams, Unidad) {
                    return Unidad.get({id : $stateParams.id});
                }]
            }
        })
        .state('unidad.new', {
            parent: 'unidad',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/unidad/unidad-dialog.html',
                    controller: 'UnidadDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nombreUnidad: null,
                                direccionUnidad: null,
                                latitudUnidad: null,
                                longitudUnidad: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('unidad', null, { reload: true });
                }, function() {
                    $state.go('unidad');
                });
            }]
        })
        .state('unidad.edit', {
            parent: 'unidad',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/unidad/unidad-dialog.html',
                    controller: 'UnidadDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Unidad', function(Unidad) {
                            return Unidad.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('unidad', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('unidad.delete', {
            parent: 'unidad',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/unidad/unidad-delete-dialog.html',
                    controller: 'UnidadDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Unidad', function(Unidad) {
                            return Unidad.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('unidad', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
