(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('UnidadDeleteController',UnidadDeleteController);

    UnidadDeleteController.$inject = ['$uibModalInstance', 'entity', 'Unidad'];

    function UnidadDeleteController($uibModalInstance, entity, Unidad) {
        var vm = this;
        vm.unidad = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Unidad.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
