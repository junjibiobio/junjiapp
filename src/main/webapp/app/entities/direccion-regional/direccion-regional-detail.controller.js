(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('DireccionRegionalDetailController', DireccionRegionalDetailController);

    DireccionRegionalDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'DireccionRegional', 'Region', 'Subdireccion', 'Jardin'];

    function DireccionRegionalDetailController($scope, $rootScope, $stateParams, entity, DireccionRegional, Region, Subdireccion, Jardin) {
        var vm = this;
        vm.direccionRegional = entity;
        vm.load = function (id) {
            DireccionRegional.get({id: id}, function(result) {
                vm.direccionRegional = result;
            });
        };
        var unsubscribe = $rootScope.$on('junjiwebApp:direccionRegionalUpdate', function(event, result) {
            vm.direccionRegional = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
