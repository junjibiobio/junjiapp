(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('DireccionRegionalDialogController', DireccionRegionalDialogController);

    DireccionRegionalDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'DireccionRegional', 'Region', 'Subdireccion', 'Jardin'];

    function DireccionRegionalDialogController ($scope, $stateParams, $uibModalInstance, $q, entity, DireccionRegional, Region, Subdireccion, Jardin) {
        var vm = this;
        vm.direccionRegional = entity;
        vm.regions = Region.query({filter: 'direccionregional-is-null'});
        $q.all([vm.direccionRegional.$promise, vm.regions.$promise]).then(function() {
            if (!vm.direccionRegional.region || !vm.direccionRegional.region.id) {
                return $q.reject();
            }
            return Region.get({id : vm.direccionRegional.region.id}).$promise;
        }).then(function(region) {
            vm.regions.push(region);
        });
        vm.subdireccions = Subdireccion.query();
        vm.jardins = Jardin.query();
        vm.load = function(id) {
            DireccionRegional.get({id : id}, function(result) {
                vm.direccionRegional = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('junjiwebApp:direccionRegionalUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.direccionRegional.id !== null) {
                DireccionRegional.update(vm.direccionRegional, onSaveSuccess, onSaveError);
            } else {
                DireccionRegional.save(vm.direccionRegional, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
