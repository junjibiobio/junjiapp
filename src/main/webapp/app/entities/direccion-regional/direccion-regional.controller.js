(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('DireccionRegionalController', DireccionRegionalController);

    DireccionRegionalController.$inject = ['$scope', '$state', 'DireccionRegional'];

    function DireccionRegionalController ($scope, $state, DireccionRegional) {
        var vm = this;
        vm.direccionRegionals = [];
        vm.loadAll = function() {
            DireccionRegional.query(function(result) {
                vm.direccionRegionals = result;
            });
        };

        vm.loadAll();
        
    }
})();
