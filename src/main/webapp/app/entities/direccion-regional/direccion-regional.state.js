(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('direccion-regional', {
            parent: 'entity',
            url: '/direccion-regional',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DireccionRegionals'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/direccion-regional/direccion-regionals.html',
                    controller: 'DireccionRegionalController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('direccion-regional-detail', {
            parent: 'entity',
            url: '/direccion-regional/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DireccionRegional'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/direccion-regional/direccion-regional-detail.html',
                    controller: 'DireccionRegionalDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DireccionRegional', function($stateParams, DireccionRegional) {
                    return DireccionRegional.get({id : $stateParams.id});
                }]
            }
        })
        .state('direccion-regional.new', {
            parent: 'direccion-regional',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/direccion-regional/direccion-regional-dialog.html',
                    controller: 'DireccionRegionalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nombreDR: null,
                                ubicacionDR: null,
                                latitudDR: null,
                                longitudDR: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('direccion-regional', null, { reload: true });
                }, function() {
                    $state.go('direccion-regional');
                });
            }]
        })
        .state('direccion-regional.edit', {
            parent: 'direccion-regional',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/direccion-regional/direccion-regional-dialog.html',
                    controller: 'DireccionRegionalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DireccionRegional', function(DireccionRegional) {
                            return DireccionRegional.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('direccion-regional', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('direccion-regional.delete', {
            parent: 'direccion-regional',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/direccion-regional/direccion-regional-delete-dialog.html',
                    controller: 'DireccionRegionalDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DireccionRegional', function(DireccionRegional) {
                            return DireccionRegional.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('direccion-regional', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
