(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('DireccionRegionalDeleteController',DireccionRegionalDeleteController);

    DireccionRegionalDeleteController.$inject = ['$uibModalInstance', 'entity', 'DireccionRegional'];

    function DireccionRegionalDeleteController($uibModalInstance, entity, DireccionRegional) {
        var vm = this;
        vm.direccionRegional = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            DireccionRegional.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
