(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('DireccionRegional', DireccionRegional);

    DireccionRegional.$inject = ['$resource'];

    function DireccionRegional ($resource) {
        var resourceUrl =  'api/direccion-regionals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
