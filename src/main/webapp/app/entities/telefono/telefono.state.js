(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('telefono', {
            parent: 'entity',
            url: '/telefono?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Telefonos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/telefono/telefonos.html',
                    controller: 'TelefonoController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('telefono-detail', {
            parent: 'entity',
            url: '/telefono/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Telefono'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/telefono/telefono-detail.html',
                    controller: 'TelefonoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Telefono', function($stateParams, Telefono) {
                    return Telefono.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'telefono',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('telefono-detail.edit', {
            parent: 'telefono-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/telefono/telefono-dialog.html',
                    controller: 'TelefonoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Telefono', function(Telefono) {
                            return Telefono.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('telefono.new', {
            parent: 'telefono',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/telefono/telefono-dialog.html',
                    controller: 'TelefonoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                tipoTelefono: null,
                                numeroTelefono: null,
                                abreviacionTelefono: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('telefono', null, { reload: true });
                }, function() {
                    $state.go('telefono');
                });
            }]
        })
        .state('telefono.edit', {
            parent: 'telefono',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/telefono/telefono-dialog.html',
                    controller: 'TelefonoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Telefono', function(Telefono) {
                            return Telefono.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('telefono', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('telefono.delete', {
            parent: 'telefono',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/telefono/telefono-delete-dialog.html',
                    controller: 'TelefonoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Telefono', function(Telefono) {
                            return Telefono.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('telefono', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
