(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('TelefonoDialogController', TelefonoDialogController);

    TelefonoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Telefono', 'Jardin', 'Funcionario'];

    function TelefonoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Telefono, Jardin, Funcionario) {
        var vm = this;

        vm.telefono = entity;
        vm.clear = clear;
        vm.save = save;
        vm.jardins = Jardin.query();
        vm.funcionarios = Funcionario.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.telefono.id !== null) {
                Telefono.update(vm.telefono, onSaveSuccess, onSaveError);
            } else {
                Telefono.save(vm.telefono, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:telefonoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
