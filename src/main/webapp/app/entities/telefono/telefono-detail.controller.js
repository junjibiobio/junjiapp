(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('TelefonoDetailController', TelefonoDetailController);

    TelefonoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Telefono', 'Jardin', 'Funcionario'];

    function TelefonoDetailController($scope, $rootScope, $stateParams, previousState, entity, Telefono, Jardin, Funcionario) {
        var vm = this;

        vm.telefono = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:telefonoUpdate', function(event, result) {
            vm.telefono = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
