(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ProcesoCSController', ProcesoCSController);

    ProcesoCSController.$inject = ['$scope', '$state', 'ProcesoCS'];

    function ProcesoCSController ($scope, $state, ProcesoCS) {
        var vm = this;
        
        vm.procesoCS = [];

        loadAll();

        function loadAll() {
            ProcesoCS.query(function(result) {
                vm.procesoCS = result;
            });
        }
    }
})();
