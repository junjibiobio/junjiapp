(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ProcesoCSDialogController', ProcesoCSDialogController);

    ProcesoCSDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProcesoCS', 'ContratoSuministro', 'Funcionario', 'DocumentacionCS'];

    function ProcesoCSDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ProcesoCS, ContratoSuministro, Funcionario, DocumentacionCS) {
        var vm = this;

        vm.procesoCS = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.contratosuministros = ContratoSuministro.query();
        vm.funcionarios = Funcionario.query();
        vm.documentacioncs = DocumentacionCS.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.procesoCS.id !== null) {
                ProcesoCS.update(vm.procesoCS, onSaveSuccess, onSaveError);
            } else {
                ProcesoCS.save(vm.procesoCS, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('junjiwebApp:procesoCSUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaProcesoCS = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
