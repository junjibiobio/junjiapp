(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ProcesoCSDetailController', ProcesoCSDetailController);

    ProcesoCSDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ProcesoCS', 'ContratoSuministro', 'Funcionario', 'DocumentacionCS'];

    function ProcesoCSDetailController($scope, $rootScope, $stateParams, previousState, entity, ProcesoCS, ContratoSuministro, Funcionario, DocumentacionCS) {
        var vm = this;

        vm.procesoCS = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('junjiwebApp:procesoCSUpdate', function(event, result) {
            vm.procesoCS = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
