(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('ProcesoCSDeleteController',ProcesoCSDeleteController);

    ProcesoCSDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProcesoCS'];

    function ProcesoCSDeleteController($uibModalInstance, entity, ProcesoCS) {
        var vm = this;

        vm.procesoCS = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ProcesoCS.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
