(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('ProcesoCS', ProcesoCS);

    ProcesoCS.$inject = ['$resource', 'DateUtils'];

    function ProcesoCS ($resource, DateUtils) {
        var resourceUrl =  'api/proceso-cs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fechaProcesoCS = DateUtils.convertDateTimeFromServer(data.fechaProcesoCS);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
