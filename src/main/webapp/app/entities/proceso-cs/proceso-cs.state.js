(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('proceso-cs', {
            parent: 'entity',
            url: '/proceso-cs',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProcesoCS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/proceso-cs/proceso-cs.html',
                    controller: 'ProcesoCSController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('proceso-cs-detail', {
            parent: 'entity',
            url: '/proceso-cs/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProcesoCS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/proceso-cs/proceso-cs-detail.html',
                    controller: 'ProcesoCSDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ProcesoCS', function($stateParams, ProcesoCS) {
                    return ProcesoCS.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'proceso-cs',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('proceso-cs-detail.edit', {
            parent: 'proceso-cs-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/proceso-cs/proceso-cs-dialog.html',
                    controller: 'ProcesoCSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProcesoCS', function(ProcesoCS) {
                            return ProcesoCS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('proceso-cs.new', {
            parent: 'proceso-cs',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/proceso-cs/proceso-cs-dialog.html',
                    controller: 'ProcesoCSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                fechaProcesoCS: null,
                                tipoProcesoCS: null,
                                estadoProcesoCS: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('proceso-cs', null, { reload: true });
                }, function() {
                    $state.go('proceso-cs');
                });
            }]
        })
        .state('proceso-cs.edit', {
            parent: 'proceso-cs',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/proceso-cs/proceso-cs-dialog.html',
                    controller: 'ProcesoCSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProcesoCS', function(ProcesoCS) {
                            return ProcesoCS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('proceso-cs', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('proceso-cs.delete', {
            parent: 'proceso-cs',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/proceso-cs/proceso-cs-delete-dialog.html',
                    controller: 'ProcesoCSDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ProcesoCS', function(ProcesoCS) {
                            return ProcesoCS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('proceso-cs', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
