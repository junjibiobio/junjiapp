(function() {
    'use strict';
    angular
        .module('junjiwebApp')
        .factory('Noticia', Noticia);

    Noticia.$inject = ['$resource'];

    function Noticia ($resource) {
        var resourceUrl =  'api/noticias/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
