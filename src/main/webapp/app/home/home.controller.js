(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService','Funcionario','ParseLinks', 'AlertService','NuevosJardines','PreJardines'];

    function HomeController ($scope, Principal, LoginService,Funcionario,ParseLinks, AlertService,NuevosJardines,PreJardines) {
        var vm = this;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        toastr.warning("Estimado(a): UTI BIOBIO es un sitio independiente, el cual pretende informar a cierto grupo,información trascendente para su trabajo diario");

        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.predicate = 'fechaContratoFuncionario';
        vm.reverse = false;


        loadAll();

        loadAllNuevosJardin();
        loadAllPreJardin();

        function loadAllNuevosJardin(){
            NuevosJardines.query(function(result){
                vm.newjardines = result;
            });
        }

        function loadAllPreJardin(){
            PreJardines.query(function(result){
                vm.prejardines = result;
            });
        }

        function loadAll () {
            Funcionario.query({
                page: 0,
                size: 10,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = 10;
                vm.funcionarios = data;
                vm.page = 0;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }


        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                loadAll();
                loadAllNuevosJardin();
                loadAllPreJardin();
            });
        };


    }
})();
