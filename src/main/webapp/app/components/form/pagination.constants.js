(function() {
    'use strict';

    angular
        .module('junjiwebApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
