/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.app.junji.web.rest.dto;
