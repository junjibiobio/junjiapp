package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.Subdireccion;
import com.app.junji.repository.SubdireccionRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Subdireccion.
 */
@RestController
@RequestMapping("/api")
public class SubdireccionResource {

    private final Logger log = LoggerFactory.getLogger(SubdireccionResource.class);
        
    @Inject
    private SubdireccionRepository subdireccionRepository;
    
    /**
     * POST  /subdireccions : Create a new subdireccion.
     *
     * @param subdireccion the subdireccion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subdireccion, or with status 400 (Bad Request) if the subdireccion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subdireccions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subdireccion> createSubdireccion(@Valid @RequestBody Subdireccion subdireccion) throws URISyntaxException {
        log.debug("REST request to save Subdireccion : {}", subdireccion);
        if (subdireccion.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("subdireccion", "idexists", "A new subdireccion cannot already have an ID")).body(null);
        }
        Subdireccion result = subdireccionRepository.save(subdireccion);
        return ResponseEntity.created(new URI("/api/subdireccions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("subdireccion", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subdireccions : Updates an existing subdireccion.
     *
     * @param subdireccion the subdireccion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subdireccion,
     * or with status 400 (Bad Request) if the subdireccion is not valid,
     * or with status 500 (Internal Server Error) if the subdireccion couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subdireccions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subdireccion> updateSubdireccion(@Valid @RequestBody Subdireccion subdireccion) throws URISyntaxException {
        log.debug("REST request to update Subdireccion : {}", subdireccion);
        if (subdireccion.getId() == null) {
            return createSubdireccion(subdireccion);
        }
        Subdireccion result = subdireccionRepository.save(subdireccion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("subdireccion", subdireccion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subdireccions : get all the subdireccions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of subdireccions in body
     */
    @RequestMapping(value = "/subdireccions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Subdireccion> getAllSubdireccions() {
        log.debug("REST request to get all Subdireccions");
        List<Subdireccion> subdireccions = subdireccionRepository.findAll();
        return subdireccions;
    }

    /**
     * GET  /subdireccions/:id : get the "id" subdireccion.
     *
     * @param id the id of the subdireccion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subdireccion, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/subdireccions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subdireccion> getSubdireccion(@PathVariable Long id) {
        log.debug("REST request to get Subdireccion : {}", id);
        Subdireccion subdireccion = subdireccionRepository.findOne(id);
        return Optional.ofNullable(subdireccion)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /subdireccions/:id : delete the "id" subdireccion.
     *
     * @param id the id of the subdireccion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/subdireccions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSubdireccion(@PathVariable Long id) {
        log.debug("REST request to delete Subdireccion : {}", id);
        subdireccionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("subdireccion", id.toString())).build();
    }

}
