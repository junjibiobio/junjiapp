package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.NivelJardin;
import com.app.junji.repository.NivelJardinRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NivelJardin.
 */
@RestController
@RequestMapping("/api")
public class NivelJardinResource {

    private final Logger log = LoggerFactory.getLogger(NivelJardinResource.class);
        
    @Inject
    private NivelJardinRepository nivelJardinRepository;
    
    /**
     * POST  /nivel-jardins : Create a new nivelJardin.
     *
     * @param nivelJardin the nivelJardin to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nivelJardin, or with status 400 (Bad Request) if the nivelJardin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/nivel-jardins",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NivelJardin> createNivelJardin(@Valid @RequestBody NivelJardin nivelJardin) throws URISyntaxException {
        log.debug("REST request to save NivelJardin : {}", nivelJardin);
        if (nivelJardin.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("nivelJardin", "idexists", "A new nivelJardin cannot already have an ID")).body(null);
        }
        NivelJardin result = nivelJardinRepository.save(nivelJardin);
        return ResponseEntity.created(new URI("/api/nivel-jardins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("nivelJardin", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nivel-jardins : Updates an existing nivelJardin.
     *
     * @param nivelJardin the nivelJardin to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nivelJardin,
     * or with status 400 (Bad Request) if the nivelJardin is not valid,
     * or with status 500 (Internal Server Error) if the nivelJardin couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/nivel-jardins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NivelJardin> updateNivelJardin(@Valid @RequestBody NivelJardin nivelJardin) throws URISyntaxException {
        log.debug("REST request to update NivelJardin : {}", nivelJardin);
        if (nivelJardin.getId() == null) {
            return createNivelJardin(nivelJardin);
        }
        NivelJardin result = nivelJardinRepository.save(nivelJardin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("nivelJardin", nivelJardin.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nivel-jardins : get all the nivelJardins.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of nivelJardins in body
     */
    @RequestMapping(value = "/nivel-jardins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<NivelJardin> getAllNivelJardins() {
        log.debug("REST request to get all NivelJardins");
        List<NivelJardin> nivelJardins = nivelJardinRepository.findAll();
        return nivelJardins;
    }

    /**
     * GET  /nivel-jardins/:id : get the "id" nivelJardin.
     *
     * @param id the id of the nivelJardin to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nivelJardin, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/nivel-jardins/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NivelJardin> getNivelJardin(@PathVariable Long id) {
        log.debug("REST request to get NivelJardin : {}", id);
        NivelJardin nivelJardin = nivelJardinRepository.findOne(id);
        return Optional.ofNullable(nivelJardin)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /nivel-jardins/:id : delete the "id" nivelJardin.
     *
     * @param id the id of the nivelJardin to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/nivel-jardins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNivelJardin(@PathVariable Long id) {
        log.debug("REST request to delete NivelJardin : {}", id);
        nivelJardinRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("nivelJardin", id.toString())).build();
    }

}
