package com.app.junji.web.rest;

import com.app.junji.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.Funcionario;
import com.app.junji.repository.FuncionarioRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Funcionario.
 */
@RestController
@RequestMapping("/api")
public class FuncionarioResource {

    private final Logger log = LoggerFactory.getLogger(FuncionarioResource.class);

    @Inject
    private FuncionarioRepository funcionarioRepository;

    /**
     * POST  /funcionarios : Create a new funcionario.
     *
     * @param funcionario the funcionario to create
     * @return the ResponseEntity with status 201 (Created) and with body the new funcionario, or with status 400 (Bad Request) if the funcionario has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/funcionarios",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Funcionario> createFuncionario(@Valid @RequestBody Funcionario funcionario) throws URISyntaxException {
        log.debug("REST request to save Funcionario : {}", funcionario);
        if (funcionario.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("funcionario", "idexists", "A new funcionario cannot already have an ID")).body(null);
        }
        Funcionario result = funcionarioRepository.save(funcionario);
        return ResponseEntity.created(new URI("/api/funcionarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("funcionario", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /funcionarios : Updates an existing funcionario.
     *
     * @param funcionario the funcionario to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated funcionario,
     * or with status 400 (Bad Request) if the funcionario is not valid,
     * or with status 500 (Internal Server Error) if the funcionario couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/funcionarios",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Funcionario> updateFuncionario(@Valid @RequestBody Funcionario funcionario) throws URISyntaxException {
        log.debug("REST request to update Funcionario : {}", funcionario);
        if (funcionario.getId() == null) {
            return createFuncionario(funcionario);
        }
        Funcionario result = funcionarioRepository.save(funcionario);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("funcionario", funcionario.getId().toString()))
            .body(result);
    }

    /**
     * GET  /funcionarios : get all the funcionarios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of funcionarios in body
     */
    @RequestMapping(value = "/funcionarios",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Funcionario>> getAllFuncionarios(Pageable pageable)
    throws URISyntaxException {
        log.debug("REST request to get all Funcionarios");
        Page<Funcionario> page = funcionarioRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/funcionarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /funcionarios : get all nuevos funcionarios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of funcionarios in body
     */
    /*
    @RequestMapping(value = "/nuevos/funcionarios",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Funcionario>> getAllNewFuncionarios(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get all Funcionarios");
        Page<Funcionario> page = funcionarioRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/funcionarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    */

    /**
     * GET  /funcionarios/:id : get the "id" funcionario.
     *
     * @param id the id of the funcionario to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the funcionario, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/funcionarios/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Funcionario> getFuncionario(@PathVariable Long id) {
        log.debug("REST request to get Funcionario : {}", id);
        Funcionario funcionario = funcionarioRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(funcionario)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /funcionarios/:id : delete the "id" funcionario.
     *
     * @param id the id of the funcionario to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/funcionarios/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFuncionario(@PathVariable Long id) {
        log.debug("REST request to delete Funcionario : {}", id);
        funcionarioRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("funcionario", id.toString())).build();
    }

}
