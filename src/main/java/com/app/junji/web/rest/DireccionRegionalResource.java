package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.DireccionRegional;
import com.app.junji.repository.DireccionRegionalRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DireccionRegional.
 */
@RestController
@RequestMapping("/api")
public class DireccionRegionalResource {

    private final Logger log = LoggerFactory.getLogger(DireccionRegionalResource.class);
        
    @Inject
    private DireccionRegionalRepository direccionRegionalRepository;
    
    /**
     * POST  /direccion-regionals : Create a new direccionRegional.
     *
     * @param direccionRegional the direccionRegional to create
     * @return the ResponseEntity with status 201 (Created) and with body the new direccionRegional, or with status 400 (Bad Request) if the direccionRegional has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/direccion-regionals",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DireccionRegional> createDireccionRegional(@Valid @RequestBody DireccionRegional direccionRegional) throws URISyntaxException {
        log.debug("REST request to save DireccionRegional : {}", direccionRegional);
        if (direccionRegional.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("direccionRegional", "idexists", "A new direccionRegional cannot already have an ID")).body(null);
        }
        DireccionRegional result = direccionRegionalRepository.save(direccionRegional);
        return ResponseEntity.created(new URI("/api/direccion-regionals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("direccionRegional", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /direccion-regionals : Updates an existing direccionRegional.
     *
     * @param direccionRegional the direccionRegional to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated direccionRegional,
     * or with status 400 (Bad Request) if the direccionRegional is not valid,
     * or with status 500 (Internal Server Error) if the direccionRegional couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/direccion-regionals",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DireccionRegional> updateDireccionRegional(@Valid @RequestBody DireccionRegional direccionRegional) throws URISyntaxException {
        log.debug("REST request to update DireccionRegional : {}", direccionRegional);
        if (direccionRegional.getId() == null) {
            return createDireccionRegional(direccionRegional);
        }
        DireccionRegional result = direccionRegionalRepository.save(direccionRegional);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("direccionRegional", direccionRegional.getId().toString()))
            .body(result);
    }

    /**
     * GET  /direccion-regionals : get all the direccionRegionals.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of direccionRegionals in body
     */
    @RequestMapping(value = "/direccion-regionals",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DireccionRegional> getAllDireccionRegionals() {
        log.debug("REST request to get all DireccionRegionals");
        List<DireccionRegional> direccionRegionals = direccionRegionalRepository.findAll();
        return direccionRegionals;
    }

    /**
     * GET  /direccion-regionals/:id : get the "id" direccionRegional.
     *
     * @param id the id of the direccionRegional to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the direccionRegional, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/direccion-regionals/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DireccionRegional> getDireccionRegional(@PathVariable Long id) {
        log.debug("REST request to get DireccionRegional : {}", id);
        DireccionRegional direccionRegional = direccionRegionalRepository.findOne(id);
        return Optional.ofNullable(direccionRegional)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /direccion-regionals/:id : delete the "id" direccionRegional.
     *
     * @param id the id of the direccionRegional to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/direccion-regionals/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDireccionRegional(@PathVariable Long id) {
        log.debug("REST request to delete DireccionRegional : {}", id);
        direccionRegionalRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("direccionRegional", id.toString())).build();
    }

}
