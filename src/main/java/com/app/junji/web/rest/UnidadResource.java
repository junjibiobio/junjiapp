package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.Unidad;
import com.app.junji.repository.UnidadRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Unidad.
 */
@RestController
@RequestMapping("/api")
public class UnidadResource {

    private final Logger log = LoggerFactory.getLogger(UnidadResource.class);
        
    @Inject
    private UnidadRepository unidadRepository;
    
    /**
     * POST  /unidads : Create a new unidad.
     *
     * @param unidad the unidad to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unidad, or with status 400 (Bad Request) if the unidad has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/unidads",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Unidad> createUnidad(@Valid @RequestBody Unidad unidad) throws URISyntaxException {
        log.debug("REST request to save Unidad : {}", unidad);
        if (unidad.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("unidad", "idexists", "A new unidad cannot already have an ID")).body(null);
        }
        Unidad result = unidadRepository.save(unidad);
        return ResponseEntity.created(new URI("/api/unidads/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("unidad", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /unidads : Updates an existing unidad.
     *
     * @param unidad the unidad to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unidad,
     * or with status 400 (Bad Request) if the unidad is not valid,
     * or with status 500 (Internal Server Error) if the unidad couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/unidads",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Unidad> updateUnidad(@Valid @RequestBody Unidad unidad) throws URISyntaxException {
        log.debug("REST request to update Unidad : {}", unidad);
        if (unidad.getId() == null) {
            return createUnidad(unidad);
        }
        Unidad result = unidadRepository.save(unidad);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("unidad", unidad.getId().toString()))
            .body(result);
    }

    /**
     * GET  /unidads : get all the unidads.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of unidads in body
     */
    @RequestMapping(value = "/unidads",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Unidad> getAllUnidads() {
        log.debug("REST request to get all Unidads");
        List<Unidad> unidads = unidadRepository.findAll();
        return unidads;
    }

    /**
     * GET  /unidads/:id : get the "id" unidad.
     *
     * @param id the id of the unidad to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unidad, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/unidads/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Unidad> getUnidad(@PathVariable Long id) {
        log.debug("REST request to get Unidad : {}", id);
        Unidad unidad = unidadRepository.findOne(id);
        return Optional.ofNullable(unidad)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /unidads/:id : delete the "id" unidad.
     *
     * @param id the id of the unidad to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/unidads/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUnidad(@PathVariable Long id) {
        log.debug("REST request to delete Unidad : {}", id);
        unidadRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("unidad", id.toString())).build();
    }

}
