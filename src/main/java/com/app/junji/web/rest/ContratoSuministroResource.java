package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.ContratoSuministro;
import com.app.junji.repository.ContratoSuministroRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import com.app.junji.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ContratoSuministro.
 */
@RestController
@RequestMapping("/api")
public class ContratoSuministroResource {

    private final Logger log = LoggerFactory.getLogger(ContratoSuministroResource.class);
        
    @Inject
    private ContratoSuministroRepository contratoSuministroRepository;
    
    /**
     * POST  /contrato-suministros : Create a new contratoSuministro.
     *
     * @param contratoSuministro the contratoSuministro to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contratoSuministro, or with status 400 (Bad Request) if the contratoSuministro has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contrato-suministros",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContratoSuministro> createContratoSuministro(@Valid @RequestBody ContratoSuministro contratoSuministro) throws URISyntaxException {
        log.debug("REST request to save ContratoSuministro : {}", contratoSuministro);
        if (contratoSuministro.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("contratoSuministro", "idexists", "A new contratoSuministro cannot already have an ID")).body(null);
        }
        ContratoSuministro result = contratoSuministroRepository.save(contratoSuministro);
        return ResponseEntity.created(new URI("/api/contrato-suministros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("contratoSuministro", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contrato-suministros : Updates an existing contratoSuministro.
     *
     * @param contratoSuministro the contratoSuministro to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contratoSuministro,
     * or with status 400 (Bad Request) if the contratoSuministro is not valid,
     * or with status 500 (Internal Server Error) if the contratoSuministro couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contrato-suministros",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContratoSuministro> updateContratoSuministro(@Valid @RequestBody ContratoSuministro contratoSuministro) throws URISyntaxException {
        log.debug("REST request to update ContratoSuministro : {}", contratoSuministro);
        if (contratoSuministro.getId() == null) {
            return createContratoSuministro(contratoSuministro);
        }
        ContratoSuministro result = contratoSuministroRepository.save(contratoSuministro);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("contratoSuministro", contratoSuministro.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contrato-suministros : get all the contratoSuministros.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of contratoSuministros in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/contrato-suministros",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ContratoSuministro>> getAllContratoSuministros(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ContratoSuministros");
        Page<ContratoSuministro> page = contratoSuministroRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contrato-suministros");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /contrato-suministros/:id : get the "id" contratoSuministro.
     *
     * @param id the id of the contratoSuministro to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contratoSuministro, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/contrato-suministros/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContratoSuministro> getContratoSuministro(@PathVariable Long id) {
        log.debug("REST request to get ContratoSuministro : {}", id);
        ContratoSuministro contratoSuministro = contratoSuministroRepository.findOne(id);
        return Optional.ofNullable(contratoSuministro)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /contrato-suministros/:id : delete the "id" contratoSuministro.
     *
     * @param id the id of the contratoSuministro to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/contrato-suministros/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteContratoSuministro(@PathVariable Long id) {
        log.debug("REST request to delete ContratoSuministro : {}", id);
        contratoSuministroRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("contratoSuministro", id.toString())).build();
    }

}
