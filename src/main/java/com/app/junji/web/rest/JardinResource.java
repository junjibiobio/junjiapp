package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.Jardin;
import com.app.junji.repository.JardinRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import com.app.junji.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Jardin.
 */
@RestController
@RequestMapping("/api")
public class JardinResource {

    private final Logger log = LoggerFactory.getLogger(JardinResource.class);

    @Inject
    private JardinRepository jardinRepository;

    /**
     * POST  /jardins : Create a new jardin.
     *
     * @param jardin the jardin to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jardin, or with status 400 (Bad Request) if the jardin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/jardins",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Jardin> createJardin(@Valid @RequestBody Jardin jardin) throws URISyntaxException {
        log.debug("REST request to save Jardin : {}", jardin);
        if (jardin.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("jardin", "idexists", "A new jardin cannot already have an ID")).body(null);
        }
        Jardin result = jardinRepository.save(jardin);
        return ResponseEntity.created(new URI("/api/jardins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("jardin", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jardins : Updates an existing jardin.
     *
     * @param jardin the jardin to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jardin,
     * or with status 400 (Bad Request) if the jardin is not valid,
     * or with status 500 (Internal Server Error) if the jardin couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/jardins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Jardin> updateJardin(@Valid @RequestBody Jardin jardin) throws URISyntaxException {
        log.debug("REST request to update Jardin : {}", jardin);
        if (jardin.getId() == null) {
            return createJardin(jardin);
        }
        Jardin result = jardinRepository.save(jardin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("jardin", jardin.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jardins : get all the jardins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jardins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/jardins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Jardin>> getAllJardins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Jardins");
        Page<Jardin> page = jardinRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jardins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jardins : get all the jardins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jardins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/jardines/nuevos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Jardin>> getAllNewJardins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Jardins");
        Page<Jardin> page = jardinRepository.findAllOrderByDateCreation(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /jardins : get all the jardins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jardins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/jardines/preinauguracion",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Jardin>> getAllPreJardins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Jardins");
        Page<Jardin> page = jardinRepository.findAllStatusPreinauguracion(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }




    /**
     * GET  /jardins : get all the jardins by search and selectValue.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jardins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/jardines/{valor}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Jardin>> getAllJardinsBySearch(@PathVariable Double valor,Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Jardins");
        Page<Jardin> page = jardinRepository.findAllBySeleccionAndValue(valor,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jardins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jardins/:id : get the "id" jardin.
     *
     * @param id the id of the jardin to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jardin, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/jardins/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Jardin> getJardin(@PathVariable Long id) {
        log.debug("REST request to get Jardin : {}", id);
        Jardin jardin = jardinRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(jardin)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /jardins/:id : delete the "id" jardin.
     *
     * @param id the id of the jardin to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/jardins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteJardin(@PathVariable Long id) {
        log.debug("REST request to delete Jardin : {}", id);
        jardinRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("jardin", id.toString())).build();
    }

}
