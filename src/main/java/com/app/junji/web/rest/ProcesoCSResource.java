package com.app.junji.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.junji.domain.ProcesoCS;
import com.app.junji.repository.ProcesoCSRepository;
import com.app.junji.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProcesoCS.
 */
@RestController
@RequestMapping("/api")
public class ProcesoCSResource {

    private final Logger log = LoggerFactory.getLogger(ProcesoCSResource.class);
        
    @Inject
    private ProcesoCSRepository procesoCSRepository;
    
    /**
     * POST  /proceso-cs : Create a new procesoCS.
     *
     * @param procesoCS the procesoCS to create
     * @return the ResponseEntity with status 201 (Created) and with body the new procesoCS, or with status 400 (Bad Request) if the procesoCS has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/proceso-cs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProcesoCS> createProcesoCS(@Valid @RequestBody ProcesoCS procesoCS) throws URISyntaxException {
        log.debug("REST request to save ProcesoCS : {}", procesoCS);
        if (procesoCS.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("procesoCS", "idexists", "A new procesoCS cannot already have an ID")).body(null);
        }
        ProcesoCS result = procesoCSRepository.save(procesoCS);
        return ResponseEntity.created(new URI("/api/proceso-cs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("procesoCS", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proceso-cs : Updates an existing procesoCS.
     *
     * @param procesoCS the procesoCS to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated procesoCS,
     * or with status 400 (Bad Request) if the procesoCS is not valid,
     * or with status 500 (Internal Server Error) if the procesoCS couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/proceso-cs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProcesoCS> updateProcesoCS(@Valid @RequestBody ProcesoCS procesoCS) throws URISyntaxException {
        log.debug("REST request to update ProcesoCS : {}", procesoCS);
        if (procesoCS.getId() == null) {
            return createProcesoCS(procesoCS);
        }
        ProcesoCS result = procesoCSRepository.save(procesoCS);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("procesoCS", procesoCS.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proceso-cs : get all the procesoCS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of procesoCS in body
     */
    @RequestMapping(value = "/proceso-cs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProcesoCS> getAllProcesoCS() {
        log.debug("REST request to get all ProcesoCS");
        List<ProcesoCS> procesoCS = procesoCSRepository.findAll();
        return procesoCS;
    }

    /**
     * GET  /proceso-cs/:id : get the "id" procesoCS.
     *
     * @param id the id of the procesoCS to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the procesoCS, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/proceso-cs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProcesoCS> getProcesoCS(@PathVariable Long id) {
        log.debug("REST request to get ProcesoCS : {}", id);
        ProcesoCS procesoCS = procesoCSRepository.findOne(id);
        return Optional.ofNullable(procesoCS)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /proceso-cs/:id : delete the "id" procesoCS.
     *
     * @param id the id of the procesoCS to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/proceso-cs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProcesoCS(@PathVariable Long id) {
        log.debug("REST request to delete ProcesoCS : {}", id);
        procesoCSRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("procesoCS", id.toString())).build();
    }

}
