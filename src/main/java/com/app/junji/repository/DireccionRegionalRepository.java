package com.app.junji.repository;

import com.app.junji.domain.DireccionRegional;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DireccionRegional entity.
 */
public interface DireccionRegionalRepository extends JpaRepository<DireccionRegional,Long> {

}
