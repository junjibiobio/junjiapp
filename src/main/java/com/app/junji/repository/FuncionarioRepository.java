package com.app.junji.repository;

import com.app.junji.domain.Funcionario;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.time.LocalDate;


/**
 * Spring Data JPA repository for the Funcionario entity.
 */
@SuppressWarnings("unused")
public interface FuncionarioRepository extends JpaRepository<Funcionario,Long> {

    @Query("select funcionario from Funcionario funcionario order by funcionario.nombresFuncionario")
    Page<Funcionario> findAllWithEagerRelationships(org.springframework.data.domain.Pageable pageable);

    @Query("select funcionario from Funcionario funcionario  order by funcionario.fechaContratoFuncionario desc")
    Page<Funcionario> findAllNew(org.springframework.data.domain.Pageable pageable);

    @Query("select funcionario from Funcionario funcionario left join fetch funcionario.telefonos where funcionario.id =:id")
    Funcionario findOneWithEagerRelationships(@Param("id") Long id);

}
