package com.app.junji.repository;

import com.app.junji.domain.Jardin;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Jardin entity.
 */
@SuppressWarnings("unused")
public interface JardinRepository extends JpaRepository<Jardin,Long> {

    @Query("select distinct jardin from Jardin jardin")
    List<Jardin> findAllWithEagerRelationships();

    @Query("select distinct jardin from Jardin jardin  where jardin.fechaCreacionJardin < current_date order by jardin.fechaCreacionJardin desc")
    Page<Jardin> findAllOrderByDateCreation(Pageable pageable);

    @Query("select distinct jardin from Jardin jardin  where jardin.estadoJardin = 'Preinauguracion'")
    Page<Jardin> findAllStatusPreinauguracion(Pageable pageable);

    @Query("select distinct jardin from Jardin jardin where jardin.codigoJardin like concat('%',:valor,'%')")
    Page<Jardin> findAllBySeleccionAndValue(@Param("valor") Double valor,Pageable pageable);

    @Query("select jardin from Jardin jardin left join fetch jardin.grupos left join fetch jardin.funcionarios where jardin.id =:id")
    Jardin findOneWithEagerRelationships(@Param("id") Long id);

}
