package com.app.junji.repository;

import com.app.junji.domain.Subdireccion;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Subdireccion entity.
 */
public interface SubdireccionRepository extends JpaRepository<Subdireccion,Long> {

}
