package com.app.junji.repository;

import com.app.junji.domain.ProcesoCS;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ProcesoCS entity.
 */
@SuppressWarnings("unused")
public interface ProcesoCSRepository extends JpaRepository<ProcesoCS,Long> {

}
