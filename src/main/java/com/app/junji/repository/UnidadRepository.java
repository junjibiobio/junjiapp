package com.app.junji.repository;

import com.app.junji.domain.Unidad;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Unidad entity.
 */
public interface UnidadRepository extends JpaRepository<Unidad,Long> {

}
