package com.app.junji.repository;

import com.app.junji.domain.NivelJardin;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the NivelJardin entity.
 */
@SuppressWarnings("unused")
public interface NivelJardinRepository extends JpaRepository<NivelJardin,Long> {

}
