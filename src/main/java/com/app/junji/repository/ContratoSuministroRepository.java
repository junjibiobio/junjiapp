package com.app.junji.repository;

import com.app.junji.domain.ContratoSuministro;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ContratoSuministro entity.
 */
@SuppressWarnings("unused")
public interface ContratoSuministroRepository extends JpaRepository<ContratoSuministro,Long> {

}
