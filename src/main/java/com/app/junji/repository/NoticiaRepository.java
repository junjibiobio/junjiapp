package com.app.junji.repository;

import com.app.junji.domain.Noticia;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Noticia entity.
 */
@SuppressWarnings("unused")
public interface NoticiaRepository extends JpaRepository<Noticia,Long> {

}
