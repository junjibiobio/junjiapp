package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Funcionario.
 */
@Entity
@Table(name = "funcionario")
public class Funcionario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 9)
    @Column(name = "rut_funcionario", nullable = false)
    private String rutFuncionario;

    @NotNull
    @Column(name = "nombres_funcionario", nullable = false)
    private String nombresFuncionario;

    @NotNull
    @Column(name = "apellidop_funcionario", nullable = false)
    private String apellidopFuncionario;

    @Column(name = "apellidom_funcionario")
    private String apellidomFuncionario;

    @Column(name = "correo_funcionario")
    private String correoFuncionario;

    @Column(name = "fecha_contrato_funcionario")
    private LocalDate fechaContratoFuncionario;

    @Column(name = "fecha_nacimiento_funcionario")
    private LocalDate fechaNacimientoFuncionario;

    @Lob
    @Column(name = "imagen_funcionario")
    private byte[] imagenFuncionario;

    @Column(name = "imagen_funcionario_content_type")
    private String imagenFuncionarioContentType;

    @Column(name = "cargo_funcionario")
    private String cargoFuncionario;

    @Column(name = "estamento_funcionario")
    private String estamentoFuncionario;

    @Column(name = "calidad_juridica_funcionario")
    private String calidadJuridicaFuncionario;

    @Column(name = "direccion_funcionario")
    private String direccionFuncionario;

    @Column(name = "codigo_funcionario")
    private String codigoFuncionario;

    @Column(name = "grado_funcionario")
    private Integer gradoFuncionario;

    @Column(name = "estado_funcionario")
    private String estadoFuncionario;

    @ManyToOne
    private Unidad unidad;

    @ManyToMany
    @JoinTable(name = "funcionario_telefono",
               joinColumns = @JoinColumn(name="funcionarios_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="telefonos_id", referencedColumnName="ID"))
    private Set<Telefono> telefonos = new HashSet<>();

    @OneToMany(mappedBy = "funcionario")
    @JsonIgnore
    private Set<ProcesoCS> procesoCS = new HashSet<>();

    @ManyToMany(mappedBy = "funcionarios")
    @JsonIgnore
    private Set<Jardin> jardins = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRutFuncionario() {
        return rutFuncionario;
    }

    public void setRutFuncionario(String rutFuncionario) {
        this.rutFuncionario = rutFuncionario;
    }

    public String getNombresFuncionario() {
        return nombresFuncionario;
    }

    public void setNombresFuncionario(String nombresFuncionario) {
        this.nombresFuncionario = nombresFuncionario;
    }

    public String getApellidopFuncionario() {
        return apellidopFuncionario;
    }

    public void setApellidopFuncionario(String apellidopFuncionario) {
        this.apellidopFuncionario = apellidopFuncionario;
    }

    public String getApellidomFuncionario() {
        return apellidomFuncionario;
    }

    public void setApellidomFuncionario(String apellidomFuncionario) {
        this.apellidomFuncionario = apellidomFuncionario;
    }

    public String getCorreoFuncionario() {
        return correoFuncionario;
    }

    public void setCorreoFuncionario(String correoFuncionario) {
        this.correoFuncionario = correoFuncionario;
    }

    public LocalDate getFechaContratoFuncionario() {
        return fechaContratoFuncionario;
    }

    public void setFechaContratoFuncionario(LocalDate fechaContratoFuncionario) {
        this.fechaContratoFuncionario = fechaContratoFuncionario;
    }

    public LocalDate getFechaNacimientoFuncionario() {
        return fechaNacimientoFuncionario;
    }

    public void setFechaNacimientoFuncionario(LocalDate fechaNacimientoFuncionario) {
        this.fechaNacimientoFuncionario = fechaNacimientoFuncionario;
    }

    public byte[] getImagenFuncionario() {
        return imagenFuncionario;
    }

    public void setImagenFuncionario(byte[] imagenFuncionario) {
        this.imagenFuncionario = imagenFuncionario;
    }

    public String getImagenFuncionarioContentType() {
        return imagenFuncionarioContentType;
    }

    public void setImagenFuncionarioContentType(String imagenFuncionarioContentType) {
        this.imagenFuncionarioContentType = imagenFuncionarioContentType;
    }

    public String getCargoFuncionario() {
        return cargoFuncionario;
    }

    public void setCargoFuncionario(String cargoFuncionario) {
        this.cargoFuncionario = cargoFuncionario;
    }

    public String getEstamentoFuncionario() {
        return estamentoFuncionario;
    }

    public void setEstamentoFuncionario(String estamentoFuncionario) {
        this.estamentoFuncionario = estamentoFuncionario;
    }

    public String getCalidadJuridicaFuncionario() {
        return calidadJuridicaFuncionario;
    }

    public void setCalidadJuridicaFuncionario(String calidadJuridicaFuncionario) {
        this.calidadJuridicaFuncionario = calidadJuridicaFuncionario;
    }

    public String getDireccionFuncionario() {
        return direccionFuncionario;
    }

    public void setDireccionFuncionario(String direccionFuncionario) {
        this.direccionFuncionario = direccionFuncionario;
    }

    public String getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(String codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    public Integer getGradoFuncionario() {
        return gradoFuncionario;
    }

    public void setGradoFuncionario(Integer gradoFuncionario) {
        this.gradoFuncionario = gradoFuncionario;
    }

    public String getEstadoFuncionario() {
        return estadoFuncionario;
    }

    public void setEstadoFuncionario(String estadoFuncionario) {
        this.estadoFuncionario = estadoFuncionario;
    }

    public Unidad getUnidad() {
        return unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public Set<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Set<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    public Set<ProcesoCS> getProcesoCS() {
        return procesoCS;
    }

    public void setProcesoCS(Set<ProcesoCS> procesoCS) {
        this.procesoCS = procesoCS;
    }

    public Set<Jardin> getJardins() {
        return jardins;
    }

    public void setJardins(Set<Jardin> jardins) {
        this.jardins = jardins;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Funcionario funcionario = (Funcionario) o;
        if(funcionario.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, funcionario.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Funcionario{" +
            "id=" + id +
            ", rutFuncionario='" + rutFuncionario + "'" +
            ", nombresFuncionario='" + nombresFuncionario + "'" +
            ", apellidopFuncionario='" + apellidopFuncionario + "'" +
            ", apellidomFuncionario='" + apellidomFuncionario + "'" +
            ", correoFuncionario='" + correoFuncionario + "'" +
            ", fechaContratoFuncionario='" + fechaContratoFuncionario + "'" +
            ", fechaNacimientoFuncionario='" + fechaNacimientoFuncionario + "'" +
            ", imagenFuncionario='" + imagenFuncionario + "'" +
            ", imagenFuncionarioContentType='" + imagenFuncionarioContentType + "'" +
            ", cargoFuncionario='" + cargoFuncionario + "'" +
            ", estamentoFuncionario='" + estamentoFuncionario + "'" +
            ", calidadJuridicaFuncionario='" + calidadJuridicaFuncionario + "'" +
            ", direccionFuncionario='" + direccionFuncionario + "'" +
            ", codigoFuncionario='" + codigoFuncionario + "'" +
            ", gradoFuncionario='" + gradoFuncionario + "'" +
            ", estadoFuncionario='" + estadoFuncionario + "'" +
            '}';
    }
}
