package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Jardin.
 */
@Entity
@Table(name = "jardin")
public class Jardin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "codigo_jardin", nullable = false)
    private Double codigoJardin;

    @NotNull
    @Column(name = "nombre_jardin", nullable = false)
    private String nombreJardin;

    @NotNull
    @Column(name = "estado_jardin", nullable = false)
    private String estadoJardin;

    @Column(name = "fecha_creacion_jardin")
    private LocalDate fechaCreacionJardin;

    @Column(name = "programa_jardin")
    private String programaJardin;

    @Column(name = "modalidad_jardin")
    private String modalidadJardin;

    @Column(name = "ruralidad_jardin")
    private String ruralidadJardin;

    @Column(name = "direccion_jardin")
    private String direccionJardin;

    @Column(name = "provincia_jardin")
    private String provinciaJardin;

    @Column(name = "comuna_jardin")
    private String comunaJardin;

    @Column(name = "latitud_jardin")
    private Double latitudJardin;

    @Column(name = "longitud_jardin")
    private Double longitudJardin;

    @ManyToOne
    private DireccionRegional direccionRegional;



    @OneToMany(mappedBy = "jardin")
    @JsonIgnore
    private Set<NivelJardin> nivelJardins = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "jardin_telefono",
               joinColumns = @JoinColumn(name="jardins_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="telefonos_id", referencedColumnName="ID"))
    private Set<Telefono> telefonos = new HashSet<>();

    @OneToMany(mappedBy = "jardin")
    @JsonIgnore
    private Set<ContratoSuministro> contratoSuministros = new HashSet<>();

    @OneToMany(mappedBy = "jardin")
    @JsonIgnore
    private Set<Grupo> grupos = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "jardin_funcionario",
               joinColumns = @JoinColumn(name="jardins_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="funcionarios_id", referencedColumnName="ID"))
    private Set<Funcionario> funcionarios = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCodigoJardin() {
        return codigoJardin;
    }

    public void setCodigoJardin(Double codigoJardin) {
        this.codigoJardin = codigoJardin;
    }

    public String getNombreJardin() {
        return nombreJardin;
    }

    public void setNombreJardin(String nombreJardin) {
        this.nombreJardin = nombreJardin;
    }

    public String getEstadoJardin() {
        return estadoJardin;
    }

    public void setEstadoJardin(String estadoJardin) {
        this.estadoJardin = estadoJardin;
    }

    public LocalDate getFechaCreacionJardin() {
        return fechaCreacionJardin;
    }

    public void setFechaCreacionJardin(LocalDate fechaCreacionJardin) {
        this.fechaCreacionJardin = fechaCreacionJardin;
    }

    public String getProgramaJardin() {
        return programaJardin;
    }

    public void setProgramaJardin(String programaJardin) {
        this.programaJardin = programaJardin;
    }

    public String getModalidadJardin() {
        return modalidadJardin;
    }

    public void setModalidadJardin(String modalidadJardin) {
        this.modalidadJardin = modalidadJardin;
    }

    public String getRuralidadJardin() {
        return ruralidadJardin;
    }

    public void setRuralidadJardin(String ruralidadJardin) {
        this.ruralidadJardin = ruralidadJardin;
    }

    public String getDireccionJardin() {
        return direccionJardin;
    }

    public void setDireccionJardin(String direccionJardin) {
        this.direccionJardin = direccionJardin;
    }

    public String getProvinciaJardin() {
        return provinciaJardin;
    }

    public void setProvinciaJardin(String provinciaJardin) {
        this.provinciaJardin = provinciaJardin;
    }

    public String getComunaJardin() {
        return comunaJardin;
    }

    public void setComunaJardin(String comunaJardin) {
        this.comunaJardin = comunaJardin;
    }

    public Double getLatitudJardin() {
        return latitudJardin;
    }

    public void setLatitudJardin(Double latitudJardin) {
        this.latitudJardin = latitudJardin;
    }

    public Double getLongitudJardin() {
        return longitudJardin;
    }

    public void setLongitudJardin(Double longitudJardin) {
        this.longitudJardin = longitudJardin;
    }

    public DireccionRegional getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(DireccionRegional direccionRegional) {
        this.direccionRegional = direccionRegional;
    }



    public Set<NivelJardin> getNivelJardins() {
        return nivelJardins;
    }

    public void setNivelJardins(Set<NivelJardin> nivelJardins) {
        this.nivelJardins = nivelJardins;
    }

    public Set<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Set<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    public Set<ContratoSuministro> getContratoSuministros() {
        return contratoSuministros;
    }

    public void setContratoSuministros(Set<ContratoSuministro> contratoSuministros) {
        this.contratoSuministros = contratoSuministros;
    }

    public Set<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(Set<Grupo> grupos) {
        this.grupos = grupos;
    }

    public Set<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Jardin jardin = (Jardin) o;
        if(jardin.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, jardin.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Jardin{" +
            "id=" + id +
            ", codigoJardin='" + codigoJardin + "'" +
            ", nombreJardin='" + nombreJardin + "'" +
            ", estadoJardin='" + estadoJardin + "'" +
            ", fechaCreacionJardin='" + fechaCreacionJardin + "'" +
            ", programaJardin='" + programaJardin + "'" +
            ", modalidadJardin='" + modalidadJardin + "'" +
            ", ruralidadJardin='" + ruralidadJardin + "'" +
            ", direccionJardin='" + direccionJardin + "'" +
            ", provinciaJardin='" + provinciaJardin + "'" +
            ", comunaJardin='" + comunaJardin + "'" +
            ", latitudJardin='" + latitudJardin + "'" +
            ", longitudJardin='" + longitudJardin + "'" +
            '}';
    }
}
