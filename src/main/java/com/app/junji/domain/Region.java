package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Region.
 */
@Entity
@Table(name = "region")
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "codigo_region", nullable = false)
    private Integer codigoRegion;

    @NotNull
    @Column(name = "nombre_region", nullable = false)
    private String nombreRegion;

    @OneToOne(mappedBy = "region")
    @JsonIgnore
    private DireccionRegional direccionRegional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodigoRegion() {
        return codigoRegion;
    }

    public void setCodigoRegion(Integer codigoRegion) {
        this.codigoRegion = codigoRegion;
    }

    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }

    public DireccionRegional getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(DireccionRegional direccionRegional) {
        this.direccionRegional = direccionRegional;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Region region = (Region) o;
        if(region.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, region.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Region{" +
            "id=" + id +
            ", codigoRegion='" + codigoRegion + "'" +
            ", nombreRegion='" + nombreRegion + "'" +
            '}';
    }
}
