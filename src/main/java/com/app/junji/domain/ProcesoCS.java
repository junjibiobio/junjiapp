package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ProcesoCS.
 */
@Entity
@Table(name = "proceso_cs")
public class ProcesoCS implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "fecha_proceso_cs")
    private ZonedDateTime fechaProcesoCS;

    @NotNull
    @Column(name = "tipo_proceso_cs", nullable = false)
    private String tipoProcesoCS;

    @Column(name = "estado_proceso_cs")
    private String estadoProcesoCS;

    @ManyToOne
    private ContratoSuministro contratoSuministro;

    @ManyToOne
    private Funcionario funcionario;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaProcesoCS() {
        return fechaProcesoCS;
    }

    public void setFechaProcesoCS(ZonedDateTime fechaProcesoCS) {
        this.fechaProcesoCS = fechaProcesoCS;
    }

    public String getTipoProcesoCS() {
        return tipoProcesoCS;
    }

    public void setTipoProcesoCS(String tipoProcesoCS) {
        this.tipoProcesoCS = tipoProcesoCS;
    }

    public String getEstadoProcesoCS() {
        return estadoProcesoCS;
    }

    public void setEstadoProcesoCS(String estadoProcesoCS) {
        this.estadoProcesoCS = estadoProcesoCS;
    }

    public ContratoSuministro getContratoSuministro() {
        return contratoSuministro;
    }

    public void setContratoSuministro(ContratoSuministro contratoSuministro) {
        this.contratoSuministro = contratoSuministro;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcesoCS procesoCS = (ProcesoCS) o;
        if(procesoCS.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, procesoCS.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProcesoCS{" +
            "id=" + id +
            ", fechaProcesoCS='" + fechaProcesoCS + "'" +
            ", tipoProcesoCS='" + tipoProcesoCS + "'" +
            ", estadoProcesoCS='" + estadoProcesoCS + "'" +
            '}';
    }
}
