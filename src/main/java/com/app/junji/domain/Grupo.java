package com.app.junji.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Grupo.
 */
@Entity
@Table(name = "grupo")
public class Grupo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre_grupo", nullable = false)
    private String nombreGrupo;

    @Column(name = "capacidad_grupo")
    private Double capacidadGrupo;

    @Column(name = "matricula_grupo")
    private Double matriculaGrupo;

    @Column(name = "asistencia_grupo")
    private Double asistenciaGrupo;

    @ManyToOne
    @JsonIgnore
    private Jardin jardin;

    @ManyToOne
    private NivelJardin nivelJardin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public Double getCapacidadGrupo() {
        return capacidadGrupo;
    }

    public void setCapacidadGrupo(Double capacidadGrupo) {
        this.capacidadGrupo = capacidadGrupo;
    }

    public Double getMatriculaGrupo() {
        return matriculaGrupo;
    }

    public void setMatriculaGrupo(Double matriculaGrupo) {
        this.matriculaGrupo = matriculaGrupo;
    }

    public Double getAsistenciaGrupo() {
        return asistenciaGrupo;
    }

    public void setAsistenciaGrupo(Double asistenciaGrupo) {
        this.asistenciaGrupo = asistenciaGrupo;
    }

    public Jardin getJardin() {
        return jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    public NivelJardin getNivelJardin() {
        return nivelJardin;
    }

    public void setNivelJardin(NivelJardin nivelJardin) {
        this.nivelJardin = nivelJardin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Grupo grupo = (Grupo) o;
        if(grupo.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, grupo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Grupo{" +
            "id=" + id +
            ", nombreGrupo='" + nombreGrupo + "'" +
            ", capacidadGrupo='" + capacidadGrupo + "'" +
            ", matriculaGrupo='" + matriculaGrupo + "'" +
            ", asistenciaGrupo='" + asistenciaGrupo + "'" +
            '}';
    }
}
