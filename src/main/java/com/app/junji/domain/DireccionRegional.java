package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DireccionRegional.
 */
@Entity
@Table(name = "direccion_regional")
public class DireccionRegional implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre_dr", nullable = false)
    private String nombreDR;

    @Column(name = "ubicacion_dr")
    private String ubicacionDR;

    @Column(name = "latitud_dr")
    private Double latitudDR;

    @Column(name = "longitud_dr")
    private Double longitudDR;

    @OneToOne
    @JoinColumn(unique = true)
    private Region region;

    @OneToMany(mappedBy = "direccionRegional")
    @JsonIgnore
    private Set<Subdireccion> subdireccions = new HashSet<>();

    @OneToMany(mappedBy = "direccionRegional")
    @JsonIgnore
    private Set<Jardin> jardins = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreDR() {
        return nombreDR;
    }

    public void setNombreDR(String nombreDR) {
        this.nombreDR = nombreDR;
    }

    public String getUbicacionDR() {
        return ubicacionDR;
    }

    public void setUbicacionDR(String ubicacionDR) {
        this.ubicacionDR = ubicacionDR;
    }

    public Double getLatitudDR() {
        return latitudDR;
    }

    public void setLatitudDR(Double latitudDR) {
        this.latitudDR = latitudDR;
    }

    public Double getLongitudDR() {
        return longitudDR;
    }

    public void setLongitudDR(Double longitudDR) {
        this.longitudDR = longitudDR;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Set<Subdireccion> getSubdireccions() {
        return subdireccions;
    }

    public void setSubdireccions(Set<Subdireccion> subdireccions) {
        this.subdireccions = subdireccions;
    }

    public Set<Jardin> getJardins() {
        return jardins;
    }

    public void setJardins(Set<Jardin> jardins) {
        this.jardins = jardins;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DireccionRegional direccionRegional = (DireccionRegional) o;
        if(direccionRegional.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, direccionRegional.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DireccionRegional{" +
            "id=" + id +
            ", nombreDR='" + nombreDR + "'" +
            ", ubicacionDR='" + ubicacionDR + "'" +
            ", latitudDR='" + latitudDR + "'" +
            ", longitudDR='" + longitudDR + "'" +
            '}';
    }
}
