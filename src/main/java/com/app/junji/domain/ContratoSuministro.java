package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ContratoSuministro.
 */
@Entity
@Table(name = "contrato_suministro")
public class ContratoSuministro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "fecha_creacion_cs")
    private ZonedDateTime fechaCreacionCS;

    @NotNull
    @Column(name = "titulo_cs", nullable = false)
    private String tituloCS;

    @Column(name = "valor_cs")
    private Double valorCS;

    @Column(name = "estado_cs")
    private String estadoCS;

    @Column(name = "tipologia_cs")
    private String tipologiaCS;

    @Column(name = "porcentaje_avance_cs")
    private Long porcentajeAvanceCS;

    @Column(name = "creado_por")
    private String creadoPor;

    @ManyToOne
    private Jardin jardin;

    @OneToMany(mappedBy = "contratoSuministro")
    @JsonIgnore
    private Set<ProcesoCS> procesoCS = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaCreacionCS() {
        return fechaCreacionCS;
    }

    public void setFechaCreacionCS(ZonedDateTime fechaCreacionCS) {
        this.fechaCreacionCS = fechaCreacionCS;
    }

    public String getTituloCS() {
        return tituloCS;
    }

    public void setTituloCS(String tituloCS) {
        this.tituloCS = tituloCS;
    }

    public Double getValorCS() {
        return valorCS;
    }

    public void setValorCS(Double valorCS) {
        this.valorCS = valorCS;
    }

    public String getEstadoCS() {
        return estadoCS;
    }

    public void setEstadoCS(String estadoCS) {
        this.estadoCS = estadoCS;
    }

    public String getTipologiaCS() {
        return tipologiaCS;
    }

    public void setTipologiaCS(String tipologiaCS) {
        this.tipologiaCS = tipologiaCS;
    }

    public Long getPorcentajeAvanceCS() {
        return porcentajeAvanceCS;
    }

    public void setPorcentajeAvanceCS(Long porcentajeAvanceCS) {
        this.porcentajeAvanceCS = porcentajeAvanceCS;
    }

    public String getCreadoPor() {
        return creadoPor;
    }

    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Jardin getJardin() {
        return jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    public Set<ProcesoCS> getProcesoCS() {
        return procesoCS;
    }

    public void setProcesoCS(Set<ProcesoCS> procesoCS) {
        this.procesoCS = procesoCS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContratoSuministro contratoSuministro = (ContratoSuministro) o;
        if(contratoSuministro.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, contratoSuministro.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ContratoSuministro{" +
            "id=" + id +
            ", fechaCreacionCS='" + fechaCreacionCS + "'" +
            ", tituloCS='" + tituloCS + "'" +
            ", valorCS='" + valorCS + "'" +
            ", estadoCS='" + estadoCS + "'" +
            ", tipologiaCS='" + tipologiaCS + "'" +
            ", porcentajeAvanceCS='" + porcentajeAvanceCS + "'" +
            ", creadoPor='" + creadoPor + "'" +
            '}';
    }
}
