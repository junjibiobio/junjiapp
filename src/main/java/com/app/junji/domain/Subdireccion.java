package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Subdireccion.
 */
@Entity
@Table(name = "subdireccion")
public class Subdireccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre_subdireccion", nullable = false)
    private String nombreSubdireccion;

    @ManyToOne
    private DireccionRegional direccionRegional;

    @OneToMany(mappedBy = "subdireccion")
    @JsonIgnore
    private Set<Unidad> unidads = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreSubdireccion() {
        return nombreSubdireccion;
    }

    public void setNombreSubdireccion(String nombreSubdireccion) {
        this.nombreSubdireccion = nombreSubdireccion;
    }

    public DireccionRegional getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(DireccionRegional direccionRegional) {
        this.direccionRegional = direccionRegional;
    }

    public Set<Unidad> getUnidads() {
        return unidads;
    }

    public void setUnidads(Set<Unidad> unidads) {
        this.unidads = unidads;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subdireccion subdireccion = (Subdireccion) o;
        if(subdireccion.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subdireccion.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Subdireccion{" +
            "id=" + id +
            ", nombreSubdireccion='" + nombreSubdireccion + "'" +
            '}';
    }
}
