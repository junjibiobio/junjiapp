package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Unidad.
 */
@Entity
@Table(name = "unidad")
public class Unidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre_unidad", nullable = false)
    private String nombreUnidad;

    @Column(name = "direccion_unidad")
    private String direccionUnidad;

    @Column(name = "latitud_unidad")
    private Double latitudUnidad;

    @Column(name = "longitud_unidad")
    private Double longitudUnidad;

    @ManyToOne
    private Subdireccion subdireccion;

    @OneToMany(mappedBy = "unidad")
    @JsonIgnore
    private Set<Funcionario> funcionarios = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreUnidad() {
        return nombreUnidad;
    }

    public void setNombreUnidad(String nombreUnidad) {
        this.nombreUnidad = nombreUnidad;
    }

    public String getDireccionUnidad() {
        return direccionUnidad;
    }

    public void setDireccionUnidad(String direccionUnidad) {
        this.direccionUnidad = direccionUnidad;
    }

    public Double getLatitudUnidad() {
        return latitudUnidad;
    }

    public void setLatitudUnidad(Double latitudUnidad) {
        this.latitudUnidad = latitudUnidad;
    }

    public Double getLongitudUnidad() {
        return longitudUnidad;
    }

    public void setLongitudUnidad(Double longitudUnidad) {
        this.longitudUnidad = longitudUnidad;
    }

    public Subdireccion getSubdireccion() {
        return subdireccion;
    }

    public void setSubdireccion(Subdireccion subdireccion) {
        this.subdireccion = subdireccion;
    }

    public Set<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Unidad unidad = (Unidad) o;
        if(unidad.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, unidad.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Unidad{" +
            "id=" + id +
            ", nombreUnidad='" + nombreUnidad + "'" +
            ", direccionUnidad='" + direccionUnidad + "'" +
            ", latitudUnidad='" + latitudUnidad + "'" +
            ", longitudUnidad='" + longitudUnidad + "'" +
            '}';
    }
}
