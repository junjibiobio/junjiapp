package com.app.junji.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Telefono.
 */
@Entity
@Table(name = "telefono")
public class Telefono implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "tipo_telefono", nullable = false)
    private String tipoTelefono;

    @Column(name = "numero_telefono")
    private String numeroTelefono;

    @Column(name = "abreviacion_telefono")
    private String abreviacionTelefono;

    @ManyToOne
    private Jardin jardin;

    @ManyToMany(mappedBy = "telefonos")
    @JsonIgnore
    private Set<Funcionario> funcionarios = new HashSet<>();

    @ManyToMany(mappedBy = "telefonos")
    @JsonIgnore
    private Set<Jardin> jardins = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(String tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getAbreviacionTelefono() {
        return abreviacionTelefono;
    }

    public void setAbreviacionTelefono(String abreviacionTelefono) {
        this.abreviacionTelefono = abreviacionTelefono;
    }

    public Jardin getJardin() {
        return jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    public Set<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public Set<Jardin> getJardins() {
        return jardins;
    }

    public void setJardins(Set<Jardin> jardins) {
        this.jardins = jardins;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Telefono telefono = (Telefono) o;
        if(telefono.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, telefono.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Telefono{" +
            "id=" + id +
            ", tipoTelefono='" + tipoTelefono + "'" +
            ", numeroTelefono='" + numeroTelefono + "'" +
            ", abreviacionTelefono='" + abreviacionTelefono + "'" +
            '}';
    }
}
