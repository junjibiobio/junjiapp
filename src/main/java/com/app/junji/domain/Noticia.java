package com.app.junji.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Noticia.
 */
@Entity
@Table(name = "noticia")
public class Noticia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "titulo_noticia")
    private String tituloNoticia;

    @Column(name = "articulo_noticia")
    private String articuloNoticia;

    @Column(name = "cuerpo_noticia")
    private String cuerpoNoticia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTituloNoticia() {
        return tituloNoticia;
    }

    public void setTituloNoticia(String tituloNoticia) {
        this.tituloNoticia = tituloNoticia;
    }

    public String getArticuloNoticia() {
        return articuloNoticia;
    }

    public void setArticuloNoticia(String articuloNoticia) {
        this.articuloNoticia = articuloNoticia;
    }

    public String getCuerpoNoticia() {
        return cuerpoNoticia;
    }

    public void setCuerpoNoticia(String cuerpoNoticia) {
        this.cuerpoNoticia = cuerpoNoticia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Noticia noticia = (Noticia) o;
        if(noticia.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, noticia.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Noticia{" +
            "id=" + id +
            ", tituloNoticia='" + tituloNoticia + "'" +
            ", articuloNoticia='" + articuloNoticia + "'" +
            ", cuerpoNoticia='" + cuerpoNoticia + "'" +
            '}';
    }
}
