package com.app.junji.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NivelJardin.
 */
@Entity
@Table(name = "nivel_jardin")
public class NivelJardin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre_nivel", nullable = false)
    private String nombreNivel;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "jardin_id")
    private Jardin jardin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreNivel() {
        return nombreNivel;
    }

    public void setNombreNivel(String nombreNivel) {
        this.nombreNivel = nombreNivel;
    }

    public Jardin getJardin() {
        return jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NivelJardin nivelJardin = (NivelJardin) o;
        if(nivelJardin.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, nivelJardin.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "NivelJardin{" +
            "id=" + id +
            ", nombreNivel='" + nombreNivel + "'" +
            '}';
    }
}
