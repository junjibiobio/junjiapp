'use strict';

describe('Controller Tests', function() {

    describe('Grupo Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockGrupo, MockJardin, MockNivelJardin;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockGrupo = jasmine.createSpy('MockGrupo');
            MockJardin = jasmine.createSpy('MockJardin');
            MockNivelJardin = jasmine.createSpy('MockNivelJardin');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Grupo': MockGrupo,
                'Jardin': MockJardin,
                'NivelJardin': MockNivelJardin
            };
            createController = function() {
                $injector.get('$controller')("GrupoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:grupoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
