'use strict';

describe('Controller Tests', function() {

    describe('Subdireccion Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSubdireccion, MockDireccionRegional, MockUnidad;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSubdireccion = jasmine.createSpy('MockSubdireccion');
            MockDireccionRegional = jasmine.createSpy('MockDireccionRegional');
            MockUnidad = jasmine.createSpy('MockUnidad');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Subdireccion': MockSubdireccion,
                'DireccionRegional': MockDireccionRegional,
                'Unidad': MockUnidad
            };
            createController = function() {
                $injector.get('$controller')("SubdireccionDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:subdireccionUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
