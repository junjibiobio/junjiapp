'use strict';

describe('Controller Tests', function() {

    describe('ContratoSuministro Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockContratoSuministro, MockJardin, MockProcesoCS;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockContratoSuministro = jasmine.createSpy('MockContratoSuministro');
            MockJardin = jasmine.createSpy('MockJardin');
            MockProcesoCS = jasmine.createSpy('MockProcesoCS');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'ContratoSuministro': MockContratoSuministro,
                'Jardin': MockJardin,
                'ProcesoCS': MockProcesoCS
            };
            createController = function() {
                $injector.get('$controller')("ContratoSuministroDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:contratoSuministroUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
