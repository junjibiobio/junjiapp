'use strict';

describe('Controller Tests', function() {

    describe('Unidad Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockUnidad, MockSubdireccion, MockFuncionario;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockUnidad = jasmine.createSpy('MockUnidad');
            MockSubdireccion = jasmine.createSpy('MockSubdireccion');
            MockFuncionario = jasmine.createSpy('MockFuncionario');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Unidad': MockUnidad,
                'Subdireccion': MockSubdireccion,
                'Funcionario': MockFuncionario
            };
            createController = function() {
                $injector.get('$controller')("UnidadDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:unidadUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
