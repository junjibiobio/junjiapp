'use strict';

describe('Controller Tests', function() {

    describe('Funcionario Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockFuncionario, MockUnidad, MockTelefono, MockProcesoCS;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockFuncionario = jasmine.createSpy('MockFuncionario');
            MockUnidad = jasmine.createSpy('MockUnidad');
            MockTelefono = jasmine.createSpy('MockTelefono');
            MockProcesoCS = jasmine.createSpy('MockProcesoCS');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Funcionario': MockFuncionario,
                'Unidad': MockUnidad,
                'Telefono': MockTelefono,
                'ProcesoCS': MockProcesoCS
            };
            createController = function() {
                $injector.get('$controller')("FuncionarioDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:funcionarioUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
