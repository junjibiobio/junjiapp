'use strict';

describe('Controller Tests', function() {

    describe('Telefono Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTelefono, MockJardin, MockFuncionario;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTelefono = jasmine.createSpy('MockTelefono');
            MockJardin = jasmine.createSpy('MockJardin');
            MockFuncionario = jasmine.createSpy('MockFuncionario');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Telefono': MockTelefono,
                'Jardin': MockJardin,
                'Funcionario': MockFuncionario
            };
            createController = function() {
                $injector.get('$controller')("TelefonoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:telefonoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
