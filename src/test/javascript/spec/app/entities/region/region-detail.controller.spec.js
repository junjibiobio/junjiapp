'use strict';

describe('Controller Tests', function() {

    describe('Region Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockRegion, MockDireccionRegional;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockRegion = jasmine.createSpy('MockRegion');
            MockDireccionRegional = jasmine.createSpy('MockDireccionRegional');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Region': MockRegion,
                'DireccionRegional': MockDireccionRegional
            };
            createController = function() {
                $injector.get('$controller')("RegionDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:regionUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
