'use strict';

describe('Controller Tests', function() {

    describe('ProcesoCS Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockProcesoCS, MockContratoSuministro, MockFuncionario, MockDocumentacionCS;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockProcesoCS = jasmine.createSpy('MockProcesoCS');
            MockContratoSuministro = jasmine.createSpy('MockContratoSuministro');
            MockFuncionario = jasmine.createSpy('MockFuncionario');
            MockDocumentacionCS = jasmine.createSpy('MockDocumentacionCS');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'ProcesoCS': MockProcesoCS,
                'ContratoSuministro': MockContratoSuministro,
                'Funcionario': MockFuncionario,
                'DocumentacionCS': MockDocumentacionCS
            };
            createController = function() {
                $injector.get('$controller')("ProcesoCSDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:procesoCSUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
