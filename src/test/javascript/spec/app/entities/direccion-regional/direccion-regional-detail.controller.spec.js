'use strict';

describe('Controller Tests', function() {

    describe('DireccionRegional Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockDireccionRegional, MockRegion, MockSubdireccion, MockJardin;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockDireccionRegional = jasmine.createSpy('MockDireccionRegional');
            MockRegion = jasmine.createSpy('MockRegion');
            MockSubdireccion = jasmine.createSpy('MockSubdireccion');
            MockJardin = jasmine.createSpy('MockJardin');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'DireccionRegional': MockDireccionRegional,
                'Region': MockRegion,
                'Subdireccion': MockSubdireccion,
                'Jardin': MockJardin
            };
            createController = function() {
                $injector.get('$controller')("DireccionRegionalDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:direccionRegionalUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
