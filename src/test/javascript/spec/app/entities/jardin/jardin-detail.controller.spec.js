'use strict';

describe('Controller Tests', function() {

    describe('Jardin Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockJardin, MockDireccionRegional, MockTelefono, MockNivelJardin, MockContratoSuministro;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockJardin = jasmine.createSpy('MockJardin');
            MockDireccionRegional = jasmine.createSpy('MockDireccionRegional');
            MockTelefono = jasmine.createSpy('MockTelefono');
            MockNivelJardin = jasmine.createSpy('MockNivelJardin');
            MockContratoSuministro = jasmine.createSpy('MockContratoSuministro');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Jardin': MockJardin,
                'DireccionRegional': MockDireccionRegional,
                'Telefono': MockTelefono,
                'NivelJardin': MockNivelJardin,
                'ContratoSuministro': MockContratoSuministro
            };
            createController = function() {
                $injector.get('$controller')("JardinDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'junjiwebApp:jardinUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
