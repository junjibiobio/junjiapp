package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Unidad;
import com.app.junji.repository.UnidadRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the UnidadResource REST controller.
 *
 * @see UnidadResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class UnidadResourceIntTest {

    private static final String DEFAULT_NOMBRE_UNIDAD = "AAAAA";
    private static final String UPDATED_NOMBRE_UNIDAD = "BBBBB";
    private static final String DEFAULT_DIRECCION_UNIDAD = "AAAAA";
    private static final String UPDATED_DIRECCION_UNIDAD = "BBBBB";

    private static final Double DEFAULT_LATITUD_UNIDAD = 1D;
    private static final Double UPDATED_LATITUD_UNIDAD = 2D;

    private static final Double DEFAULT_LONGITUD_UNIDAD = 1D;
    private static final Double UPDATED_LONGITUD_UNIDAD = 2D;

    @Inject
    private UnidadRepository unidadRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUnidadMockMvc;

    private Unidad unidad;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UnidadResource unidadResource = new UnidadResource();
        ReflectionTestUtils.setField(unidadResource, "unidadRepository", unidadRepository);
        this.restUnidadMockMvc = MockMvcBuilders.standaloneSetup(unidadResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        unidad = new Unidad();
        unidad.setNombreUnidad(DEFAULT_NOMBRE_UNIDAD);
        unidad.setDireccionUnidad(DEFAULT_DIRECCION_UNIDAD);
        unidad.setLatitudUnidad(DEFAULT_LATITUD_UNIDAD);
        unidad.setLongitudUnidad(DEFAULT_LONGITUD_UNIDAD);
    }

    @Test
    @Transactional
    public void createUnidad() throws Exception {
        int databaseSizeBeforeCreate = unidadRepository.findAll().size();

        // Create the Unidad

        restUnidadMockMvc.perform(post("/api/unidads")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(unidad)))
                .andExpect(status().isCreated());

        // Validate the Unidad in the database
        List<Unidad> unidads = unidadRepository.findAll();
        assertThat(unidads).hasSize(databaseSizeBeforeCreate + 1);
        Unidad testUnidad = unidads.get(unidads.size() - 1);
        assertThat(testUnidad.getNombreUnidad()).isEqualTo(DEFAULT_NOMBRE_UNIDAD);
        assertThat(testUnidad.getDireccionUnidad()).isEqualTo(DEFAULT_DIRECCION_UNIDAD);
        assertThat(testUnidad.getLatitudUnidad()).isEqualTo(DEFAULT_LATITUD_UNIDAD);
        assertThat(testUnidad.getLongitudUnidad()).isEqualTo(DEFAULT_LONGITUD_UNIDAD);
    }

    @Test
    @Transactional
    public void checkNombreUnidadIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadRepository.findAll().size();
        // set the field null
        unidad.setNombreUnidad(null);

        // Create the Unidad, which fails.

        restUnidadMockMvc.perform(post("/api/unidads")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(unidad)))
                .andExpect(status().isBadRequest());

        List<Unidad> unidads = unidadRepository.findAll();
        assertThat(unidads).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUnidads() throws Exception {
        // Initialize the database
        unidadRepository.saveAndFlush(unidad);

        // Get all the unidads
        restUnidadMockMvc.perform(get("/api/unidads?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(unidad.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombreUnidad").value(hasItem(DEFAULT_NOMBRE_UNIDAD.toString())))
                .andExpect(jsonPath("$.[*].direccionUnidad").value(hasItem(DEFAULT_DIRECCION_UNIDAD.toString())))
                .andExpect(jsonPath("$.[*].latitudUnidad").value(hasItem(DEFAULT_LATITUD_UNIDAD.doubleValue())))
                .andExpect(jsonPath("$.[*].longitudUnidad").value(hasItem(DEFAULT_LONGITUD_UNIDAD.doubleValue())));
    }

    @Test
    @Transactional
    public void getUnidad() throws Exception {
        // Initialize the database
        unidadRepository.saveAndFlush(unidad);

        // Get the unidad
        restUnidadMockMvc.perform(get("/api/unidads/{id}", unidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(unidad.getId().intValue()))
            .andExpect(jsonPath("$.nombreUnidad").value(DEFAULT_NOMBRE_UNIDAD.toString()))
            .andExpect(jsonPath("$.direccionUnidad").value(DEFAULT_DIRECCION_UNIDAD.toString()))
            .andExpect(jsonPath("$.latitudUnidad").value(DEFAULT_LATITUD_UNIDAD.doubleValue()))
            .andExpect(jsonPath("$.longitudUnidad").value(DEFAULT_LONGITUD_UNIDAD.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUnidad() throws Exception {
        // Get the unidad
        restUnidadMockMvc.perform(get("/api/unidads/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnidad() throws Exception {
        // Initialize the database
        unidadRepository.saveAndFlush(unidad);
        int databaseSizeBeforeUpdate = unidadRepository.findAll().size();

        // Update the unidad
        Unidad updatedUnidad = new Unidad();
        updatedUnidad.setId(unidad.getId());
        updatedUnidad.setNombreUnidad(UPDATED_NOMBRE_UNIDAD);
        updatedUnidad.setDireccionUnidad(UPDATED_DIRECCION_UNIDAD);
        updatedUnidad.setLatitudUnidad(UPDATED_LATITUD_UNIDAD);
        updatedUnidad.setLongitudUnidad(UPDATED_LONGITUD_UNIDAD);

        restUnidadMockMvc.perform(put("/api/unidads")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedUnidad)))
                .andExpect(status().isOk());

        // Validate the Unidad in the database
        List<Unidad> unidads = unidadRepository.findAll();
        assertThat(unidads).hasSize(databaseSizeBeforeUpdate);
        Unidad testUnidad = unidads.get(unidads.size() - 1);
        assertThat(testUnidad.getNombreUnidad()).isEqualTo(UPDATED_NOMBRE_UNIDAD);
        assertThat(testUnidad.getDireccionUnidad()).isEqualTo(UPDATED_DIRECCION_UNIDAD);
        assertThat(testUnidad.getLatitudUnidad()).isEqualTo(UPDATED_LATITUD_UNIDAD);
        assertThat(testUnidad.getLongitudUnidad()).isEqualTo(UPDATED_LONGITUD_UNIDAD);
    }

    @Test
    @Transactional
    public void deleteUnidad() throws Exception {
        // Initialize the database
        unidadRepository.saveAndFlush(unidad);
        int databaseSizeBeforeDelete = unidadRepository.findAll().size();

        // Get the unidad
        restUnidadMockMvc.perform(delete("/api/unidads/{id}", unidad.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Unidad> unidads = unidadRepository.findAll();
        assertThat(unidads).hasSize(databaseSizeBeforeDelete - 1);
    }
}
