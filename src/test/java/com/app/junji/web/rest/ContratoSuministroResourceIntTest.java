package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.ContratoSuministro;
import com.app.junji.repository.ContratoSuministroRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ContratoSuministroResource REST controller.
 *
 * @see ContratoSuministroResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class ContratoSuministroResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_FECHA_CREACION_CS = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_FECHA_CREACION_CS = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_FECHA_CREACION_CS_STR = dateTimeFormatter.format(DEFAULT_FECHA_CREACION_CS);
    private static final String DEFAULT_TITULO_CS = "AAAAA";
    private static final String UPDATED_TITULO_CS = "BBBBB";

    private static final Double DEFAULT_VALOR_CS = 1D;
    private static final Double UPDATED_VALOR_CS = 2D;
    private static final String DEFAULT_ESTADO_CS = "AAAAA";
    private static final String UPDATED_ESTADO_CS = "BBBBB";
    private static final String DEFAULT_TIPOLOGIA_CS = "AAAAA";
    private static final String UPDATED_TIPOLOGIA_CS = "BBBBB";

    private static final Long DEFAULT_PORCENTAJE_AVANCE_CS = 1L;
    private static final Long UPDATED_PORCENTAJE_AVANCE_CS = 2L;
    private static final String DEFAULT_CREADO_POR = "AAAAA";
    private static final String UPDATED_CREADO_POR = "BBBBB";

    @Inject
    private ContratoSuministroRepository contratoSuministroRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restContratoSuministroMockMvc;

    private ContratoSuministro contratoSuministro;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ContratoSuministroResource contratoSuministroResource = new ContratoSuministroResource();
        ReflectionTestUtils.setField(contratoSuministroResource, "contratoSuministroRepository", contratoSuministroRepository);
        this.restContratoSuministroMockMvc = MockMvcBuilders.standaloneSetup(contratoSuministroResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        contratoSuministro = new ContratoSuministro();
        contratoSuministro.setFechaCreacionCS(DEFAULT_FECHA_CREACION_CS);
        contratoSuministro.setTituloCS(DEFAULT_TITULO_CS);
        contratoSuministro.setValorCS(DEFAULT_VALOR_CS);
        contratoSuministro.setEstadoCS(DEFAULT_ESTADO_CS);
        contratoSuministro.setTipologiaCS(DEFAULT_TIPOLOGIA_CS);
        contratoSuministro.setPorcentajeAvanceCS(DEFAULT_PORCENTAJE_AVANCE_CS);
        contratoSuministro.setCreadoPor(DEFAULT_CREADO_POR);
    }

    @Test
    @Transactional
    public void createContratoSuministro() throws Exception {
        int databaseSizeBeforeCreate = contratoSuministroRepository.findAll().size();

        // Create the ContratoSuministro

        restContratoSuministroMockMvc.perform(post("/api/contrato-suministros")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contratoSuministro)))
                .andExpect(status().isCreated());

        // Validate the ContratoSuministro in the database
        List<ContratoSuministro> contratoSuministros = contratoSuministroRepository.findAll();
        assertThat(contratoSuministros).hasSize(databaseSizeBeforeCreate + 1);
        ContratoSuministro testContratoSuministro = contratoSuministros.get(contratoSuministros.size() - 1);
        assertThat(testContratoSuministro.getFechaCreacionCS()).isEqualTo(DEFAULT_FECHA_CREACION_CS);
        assertThat(testContratoSuministro.getTituloCS()).isEqualTo(DEFAULT_TITULO_CS);
        assertThat(testContratoSuministro.getValorCS()).isEqualTo(DEFAULT_VALOR_CS);
        assertThat(testContratoSuministro.getEstadoCS()).isEqualTo(DEFAULT_ESTADO_CS);
        assertThat(testContratoSuministro.getTipologiaCS()).isEqualTo(DEFAULT_TIPOLOGIA_CS);
        assertThat(testContratoSuministro.getPorcentajeAvanceCS()).isEqualTo(DEFAULT_PORCENTAJE_AVANCE_CS);
        assertThat(testContratoSuministro.getCreadoPor()).isEqualTo(DEFAULT_CREADO_POR);
    }

    @Test
    @Transactional
    public void checkTituloCSIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoSuministroRepository.findAll().size();
        // set the field null
        contratoSuministro.setTituloCS(null);

        // Create the ContratoSuministro, which fails.

        restContratoSuministroMockMvc.perform(post("/api/contrato-suministros")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contratoSuministro)))
                .andExpect(status().isBadRequest());

        List<ContratoSuministro> contratoSuministros = contratoSuministroRepository.findAll();
        assertThat(contratoSuministros).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContratoSuministros() throws Exception {
        // Initialize the database
        contratoSuministroRepository.saveAndFlush(contratoSuministro);

        // Get all the contratoSuministros
        restContratoSuministroMockMvc.perform(get("/api/contrato-suministros?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(contratoSuministro.getId().intValue())))
                .andExpect(jsonPath("$.[*].fechaCreacionCS").value(hasItem(DEFAULT_FECHA_CREACION_CS_STR)))
                .andExpect(jsonPath("$.[*].tituloCS").value(hasItem(DEFAULT_TITULO_CS.toString())))
                .andExpect(jsonPath("$.[*].valorCS").value(hasItem(DEFAULT_VALOR_CS.doubleValue())))
                .andExpect(jsonPath("$.[*].estadoCS").value(hasItem(DEFAULT_ESTADO_CS.toString())))
                .andExpect(jsonPath("$.[*].tipologiaCS").value(hasItem(DEFAULT_TIPOLOGIA_CS.toString())))
                .andExpect(jsonPath("$.[*].porcentajeAvanceCS").value(hasItem(DEFAULT_PORCENTAJE_AVANCE_CS.intValue())))
                .andExpect(jsonPath("$.[*].creadoPor").value(hasItem(DEFAULT_CREADO_POR.toString())));
    }

    @Test
    @Transactional
    public void getContratoSuministro() throws Exception {
        // Initialize the database
        contratoSuministroRepository.saveAndFlush(contratoSuministro);

        // Get the contratoSuministro
        restContratoSuministroMockMvc.perform(get("/api/contrato-suministros/{id}", contratoSuministro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(contratoSuministro.getId().intValue()))
            .andExpect(jsonPath("$.fechaCreacionCS").value(DEFAULT_FECHA_CREACION_CS_STR))
            .andExpect(jsonPath("$.tituloCS").value(DEFAULT_TITULO_CS.toString()))
            .andExpect(jsonPath("$.valorCS").value(DEFAULT_VALOR_CS.doubleValue()))
            .andExpect(jsonPath("$.estadoCS").value(DEFAULT_ESTADO_CS.toString()))
            .andExpect(jsonPath("$.tipologiaCS").value(DEFAULT_TIPOLOGIA_CS.toString()))
            .andExpect(jsonPath("$.porcentajeAvanceCS").value(DEFAULT_PORCENTAJE_AVANCE_CS.intValue()))
            .andExpect(jsonPath("$.creadoPor").value(DEFAULT_CREADO_POR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingContratoSuministro() throws Exception {
        // Get the contratoSuministro
        restContratoSuministroMockMvc.perform(get("/api/contrato-suministros/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContratoSuministro() throws Exception {
        // Initialize the database
        contratoSuministroRepository.saveAndFlush(contratoSuministro);
        int databaseSizeBeforeUpdate = contratoSuministroRepository.findAll().size();

        // Update the contratoSuministro
        ContratoSuministro updatedContratoSuministro = new ContratoSuministro();
        updatedContratoSuministro.setId(contratoSuministro.getId());
        updatedContratoSuministro.setFechaCreacionCS(UPDATED_FECHA_CREACION_CS);
        updatedContratoSuministro.setTituloCS(UPDATED_TITULO_CS);
        updatedContratoSuministro.setValorCS(UPDATED_VALOR_CS);
        updatedContratoSuministro.setEstadoCS(UPDATED_ESTADO_CS);
        updatedContratoSuministro.setTipologiaCS(UPDATED_TIPOLOGIA_CS);
        updatedContratoSuministro.setPorcentajeAvanceCS(UPDATED_PORCENTAJE_AVANCE_CS);
        updatedContratoSuministro.setCreadoPor(UPDATED_CREADO_POR);

        restContratoSuministroMockMvc.perform(put("/api/contrato-suministros")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedContratoSuministro)))
                .andExpect(status().isOk());

        // Validate the ContratoSuministro in the database
        List<ContratoSuministro> contratoSuministros = contratoSuministroRepository.findAll();
        assertThat(contratoSuministros).hasSize(databaseSizeBeforeUpdate);
        ContratoSuministro testContratoSuministro = contratoSuministros.get(contratoSuministros.size() - 1);
        assertThat(testContratoSuministro.getFechaCreacionCS()).isEqualTo(UPDATED_FECHA_CREACION_CS);
        assertThat(testContratoSuministro.getTituloCS()).isEqualTo(UPDATED_TITULO_CS);
        assertThat(testContratoSuministro.getValorCS()).isEqualTo(UPDATED_VALOR_CS);
        assertThat(testContratoSuministro.getEstadoCS()).isEqualTo(UPDATED_ESTADO_CS);
        assertThat(testContratoSuministro.getTipologiaCS()).isEqualTo(UPDATED_TIPOLOGIA_CS);
        assertThat(testContratoSuministro.getPorcentajeAvanceCS()).isEqualTo(UPDATED_PORCENTAJE_AVANCE_CS);
        assertThat(testContratoSuministro.getCreadoPor()).isEqualTo(UPDATED_CREADO_POR);
    }

    @Test
    @Transactional
    public void deleteContratoSuministro() throws Exception {
        // Initialize the database
        contratoSuministroRepository.saveAndFlush(contratoSuministro);
        int databaseSizeBeforeDelete = contratoSuministroRepository.findAll().size();

        // Get the contratoSuministro
        restContratoSuministroMockMvc.perform(delete("/api/contrato-suministros/{id}", contratoSuministro.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ContratoSuministro> contratoSuministros = contratoSuministroRepository.findAll();
        assertThat(contratoSuministros).hasSize(databaseSizeBeforeDelete - 1);
    }
}
