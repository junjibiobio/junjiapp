package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.NivelJardin;
import com.app.junji.repository.NivelJardinRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the NivelJardinResource REST controller.
 *
 * @see NivelJardinResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class NivelJardinResourceIntTest {

    private static final String DEFAULT_NOMBRE_NIVEL = "AAAAA";
    private static final String UPDATED_NOMBRE_NIVEL = "BBBBB";

    @Inject
    private NivelJardinRepository nivelJardinRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restNivelJardinMockMvc;

    private NivelJardin nivelJardin;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NivelJardinResource nivelJardinResource = new NivelJardinResource();
        ReflectionTestUtils.setField(nivelJardinResource, "nivelJardinRepository", nivelJardinRepository);
        this.restNivelJardinMockMvc = MockMvcBuilders.standaloneSetup(nivelJardinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        nivelJardin = new NivelJardin();
        nivelJardin.setNombreNivel(DEFAULT_NOMBRE_NIVEL);
    }

    @Test
    @Transactional
    public void createNivelJardin() throws Exception {
        int databaseSizeBeforeCreate = nivelJardinRepository.findAll().size();

        // Create the NivelJardin

        restNivelJardinMockMvc.perform(post("/api/nivel-jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(nivelJardin)))
                .andExpect(status().isCreated());

        // Validate the NivelJardin in the database
        List<NivelJardin> nivelJardins = nivelJardinRepository.findAll();
        assertThat(nivelJardins).hasSize(databaseSizeBeforeCreate + 1);
        NivelJardin testNivelJardin = nivelJardins.get(nivelJardins.size() - 1);
        assertThat(testNivelJardin.getNombreNivel()).isEqualTo(DEFAULT_NOMBRE_NIVEL);
    }

    @Test
    @Transactional
    public void checkNombreNivelIsRequired() throws Exception {
        int databaseSizeBeforeTest = nivelJardinRepository.findAll().size();
        // set the field null
        nivelJardin.setNombreNivel(null);

        // Create the NivelJardin, which fails.

        restNivelJardinMockMvc.perform(post("/api/nivel-jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(nivelJardin)))
                .andExpect(status().isBadRequest());

        List<NivelJardin> nivelJardins = nivelJardinRepository.findAll();
        assertThat(nivelJardins).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNivelJardins() throws Exception {
        // Initialize the database
        nivelJardinRepository.saveAndFlush(nivelJardin);

        // Get all the nivelJardins
        restNivelJardinMockMvc.perform(get("/api/nivel-jardins?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(nivelJardin.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombreNivel").value(hasItem(DEFAULT_NOMBRE_NIVEL.toString())));
    }

    @Test
    @Transactional
    public void getNivelJardin() throws Exception {
        // Initialize the database
        nivelJardinRepository.saveAndFlush(nivelJardin);

        // Get the nivelJardin
        restNivelJardinMockMvc.perform(get("/api/nivel-jardins/{id}", nivelJardin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(nivelJardin.getId().intValue()))
            .andExpect(jsonPath("$.nombreNivel").value(DEFAULT_NOMBRE_NIVEL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNivelJardin() throws Exception {
        // Get the nivelJardin
        restNivelJardinMockMvc.perform(get("/api/nivel-jardins/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNivelJardin() throws Exception {
        // Initialize the database
        nivelJardinRepository.saveAndFlush(nivelJardin);
        int databaseSizeBeforeUpdate = nivelJardinRepository.findAll().size();

        // Update the nivelJardin
        NivelJardin updatedNivelJardin = new NivelJardin();
        updatedNivelJardin.setId(nivelJardin.getId());
        updatedNivelJardin.setNombreNivel(UPDATED_NOMBRE_NIVEL);

        restNivelJardinMockMvc.perform(put("/api/nivel-jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNivelJardin)))
                .andExpect(status().isOk());

        // Validate the NivelJardin in the database
        List<NivelJardin> nivelJardins = nivelJardinRepository.findAll();
        assertThat(nivelJardins).hasSize(databaseSizeBeforeUpdate);
        NivelJardin testNivelJardin = nivelJardins.get(nivelJardins.size() - 1);
        assertThat(testNivelJardin.getNombreNivel()).isEqualTo(UPDATED_NOMBRE_NIVEL);
    }

    @Test
    @Transactional
    public void deleteNivelJardin() throws Exception {
        // Initialize the database
        nivelJardinRepository.saveAndFlush(nivelJardin);
        int databaseSizeBeforeDelete = nivelJardinRepository.findAll().size();

        // Get the nivelJardin
        restNivelJardinMockMvc.perform(delete("/api/nivel-jardins/{id}", nivelJardin.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<NivelJardin> nivelJardins = nivelJardinRepository.findAll();
        assertThat(nivelJardins).hasSize(databaseSizeBeforeDelete - 1);
    }
}
