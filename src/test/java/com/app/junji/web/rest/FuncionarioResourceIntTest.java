package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Funcionario;
import com.app.junji.repository.FuncionarioRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FuncionarioResource REST controller.
 *
 * @see FuncionarioResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class FuncionarioResourceIntTest {

    private static final String DEFAULT_RUT_FUNCIONARIO = "AAAAAAAAA";
    private static final String UPDATED_RUT_FUNCIONARIO = "BBBBBBBBB";
    private static final String DEFAULT_NOMBRES_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_NOMBRES_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_APELLIDOP_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_APELLIDOP_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_APELLIDOM_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_APELLIDOM_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_CORREO_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_CORREO_FUNCIONARIO = "BBBBB";

    private static final LocalDate DEFAULT_FECHA_CONTRATO_FUNCIONARIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_CONTRATO_FUNCIONARIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FECHA_NACIMIENTO_FUNCIONARIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_NACIMIENTO_FUNCIONARIO = LocalDate.now(ZoneId.systemDefault());

    private static final byte[] DEFAULT_IMAGEN_FUNCIONARIO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGEN_FUNCIONARIO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGEN_FUNCIONARIO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGEN_FUNCIONARIO_CONTENT_TYPE = "image/png";
    private static final String DEFAULT_CARGO_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_CARGO_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_ESTAMENTO_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_ESTAMENTO_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_CALIDAD_JURIDICA_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_CALIDAD_JURIDICA_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_DIRECCION_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_DIRECCION_FUNCIONARIO = "BBBBB";
    private static final String DEFAULT_CODIGO_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_CODIGO_FUNCIONARIO = "BBBBB";

    private static final Integer DEFAULT_GRADO_FUNCIONARIO = 1;
    private static final Integer UPDATED_GRADO_FUNCIONARIO = 2;
    private static final String DEFAULT_ESTADO_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_ESTADO_FUNCIONARIO = "BBBBB";

    @Inject
    private FuncionarioRepository funcionarioRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFuncionarioMockMvc;

    private Funcionario funcionario;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FuncionarioResource funcionarioResource = new FuncionarioResource();
        ReflectionTestUtils.setField(funcionarioResource, "funcionarioRepository", funcionarioRepository);
        this.restFuncionarioMockMvc = MockMvcBuilders.standaloneSetup(funcionarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        funcionario = new Funcionario();
        funcionario.setRutFuncionario(DEFAULT_RUT_FUNCIONARIO);
        funcionario.setNombresFuncionario(DEFAULT_NOMBRES_FUNCIONARIO);
        funcionario.setApellidopFuncionario(DEFAULT_APELLIDOP_FUNCIONARIO);
        funcionario.setApellidomFuncionario(DEFAULT_APELLIDOM_FUNCIONARIO);
        funcionario.setCorreoFuncionario(DEFAULT_CORREO_FUNCIONARIO);
        funcionario.setFechaContratoFuncionario(DEFAULT_FECHA_CONTRATO_FUNCIONARIO);
        funcionario.setFechaNacimientoFuncionario(DEFAULT_FECHA_NACIMIENTO_FUNCIONARIO);
        funcionario.setImagenFuncionario(DEFAULT_IMAGEN_FUNCIONARIO);
        funcionario.setImagenFuncionarioContentType(DEFAULT_IMAGEN_FUNCIONARIO_CONTENT_TYPE);
        funcionario.setCargoFuncionario(DEFAULT_CARGO_FUNCIONARIO);
        funcionario.setEstamentoFuncionario(DEFAULT_ESTAMENTO_FUNCIONARIO);
        funcionario.setCalidadJuridicaFuncionario(DEFAULT_CALIDAD_JURIDICA_FUNCIONARIO);
        funcionario.setDireccionFuncionario(DEFAULT_DIRECCION_FUNCIONARIO);
        funcionario.setCodigoFuncionario(DEFAULT_CODIGO_FUNCIONARIO);
        funcionario.setGradoFuncionario(DEFAULT_GRADO_FUNCIONARIO);
        funcionario.setEstadoFuncionario(DEFAULT_ESTADO_FUNCIONARIO);
    }

    @Test
    @Transactional
    public void createFuncionario() throws Exception {
        int databaseSizeBeforeCreate = funcionarioRepository.findAll().size();

        // Create the Funcionario

        restFuncionarioMockMvc.perform(post("/api/funcionarios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(funcionario)))
                .andExpect(status().isCreated());

        // Validate the Funcionario in the database
        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeCreate + 1);
        Funcionario testFuncionario = funcionarios.get(funcionarios.size() - 1);
        assertThat(testFuncionario.getRutFuncionario()).isEqualTo(DEFAULT_RUT_FUNCIONARIO);
        assertThat(testFuncionario.getNombresFuncionario()).isEqualTo(DEFAULT_NOMBRES_FUNCIONARIO);
        assertThat(testFuncionario.getApellidopFuncionario()).isEqualTo(DEFAULT_APELLIDOP_FUNCIONARIO);
        assertThat(testFuncionario.getApellidomFuncionario()).isEqualTo(DEFAULT_APELLIDOM_FUNCIONARIO);
        assertThat(testFuncionario.getCorreoFuncionario()).isEqualTo(DEFAULT_CORREO_FUNCIONARIO);
        assertThat(testFuncionario.getFechaContratoFuncionario()).isEqualTo(DEFAULT_FECHA_CONTRATO_FUNCIONARIO);
        assertThat(testFuncionario.getFechaNacimientoFuncionario()).isEqualTo(DEFAULT_FECHA_NACIMIENTO_FUNCIONARIO);
        assertThat(testFuncionario.getImagenFuncionario()).isEqualTo(DEFAULT_IMAGEN_FUNCIONARIO);
        assertThat(testFuncionario.getImagenFuncionarioContentType()).isEqualTo(DEFAULT_IMAGEN_FUNCIONARIO_CONTENT_TYPE);
        assertThat(testFuncionario.getCargoFuncionario()).isEqualTo(DEFAULT_CARGO_FUNCIONARIO);
        assertThat(testFuncionario.getEstamentoFuncionario()).isEqualTo(DEFAULT_ESTAMENTO_FUNCIONARIO);
        assertThat(testFuncionario.getCalidadJuridicaFuncionario()).isEqualTo(DEFAULT_CALIDAD_JURIDICA_FUNCIONARIO);
        assertThat(testFuncionario.getDireccionFuncionario()).isEqualTo(DEFAULT_DIRECCION_FUNCIONARIO);
        assertThat(testFuncionario.getCodigoFuncionario()).isEqualTo(DEFAULT_CODIGO_FUNCIONARIO);
        assertThat(testFuncionario.getGradoFuncionario()).isEqualTo(DEFAULT_GRADO_FUNCIONARIO);
        assertThat(testFuncionario.getEstadoFuncionario()).isEqualTo(DEFAULT_ESTADO_FUNCIONARIO);
    }

    @Test
    @Transactional
    public void checkRutFuncionarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = funcionarioRepository.findAll().size();
        // set the field null
        funcionario.setRutFuncionario(null);

        // Create the Funcionario, which fails.

        restFuncionarioMockMvc.perform(post("/api/funcionarios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(funcionario)))
                .andExpect(status().isBadRequest());

        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNombresFuncionarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = funcionarioRepository.findAll().size();
        // set the field null
        funcionario.setNombresFuncionario(null);

        // Create the Funcionario, which fails.

        restFuncionarioMockMvc.perform(post("/api/funcionarios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(funcionario)))
                .andExpect(status().isBadRequest());

        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkApellidopFuncionarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = funcionarioRepository.findAll().size();
        // set the field null
        funcionario.setApellidopFuncionario(null);

        // Create the Funcionario, which fails.

        restFuncionarioMockMvc.perform(post("/api/funcionarios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(funcionario)))
                .andExpect(status().isBadRequest());

        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFuncionarios() throws Exception {
        // Initialize the database
        funcionarioRepository.saveAndFlush(funcionario);

        // Get all the funcionarios
        restFuncionarioMockMvc.perform(get("/api/funcionarios?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(funcionario.getId().intValue())))
                .andExpect(jsonPath("$.[*].rutFuncionario").value(hasItem(DEFAULT_RUT_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].nombresFuncionario").value(hasItem(DEFAULT_NOMBRES_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].apellidopFuncionario").value(hasItem(DEFAULT_APELLIDOP_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].apellidomFuncionario").value(hasItem(DEFAULT_APELLIDOM_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].correoFuncionario").value(hasItem(DEFAULT_CORREO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].fechaContratoFuncionario").value(hasItem(DEFAULT_FECHA_CONTRATO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].fechaNacimientoFuncionario").value(hasItem(DEFAULT_FECHA_NACIMIENTO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].imagenFuncionarioContentType").value(hasItem(DEFAULT_IMAGEN_FUNCIONARIO_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].imagenFuncionario").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGEN_FUNCIONARIO))))
                .andExpect(jsonPath("$.[*].cargoFuncionario").value(hasItem(DEFAULT_CARGO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].estamentoFuncionario").value(hasItem(DEFAULT_ESTAMENTO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].calidadJuridicaFuncionario").value(hasItem(DEFAULT_CALIDAD_JURIDICA_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].direccionFuncionario").value(hasItem(DEFAULT_DIRECCION_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].codigoFuncionario").value(hasItem(DEFAULT_CODIGO_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].gradoFuncionario").value(hasItem(DEFAULT_GRADO_FUNCIONARIO)))
                .andExpect(jsonPath("$.[*].estadoFuncionario").value(hasItem(DEFAULT_ESTADO_FUNCIONARIO.toString())));
    }

    @Test
    @Transactional
    public void getFuncionario() throws Exception {
        // Initialize the database
        funcionarioRepository.saveAndFlush(funcionario);

        // Get the funcionario
        restFuncionarioMockMvc.perform(get("/api/funcionarios/{id}", funcionario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(funcionario.getId().intValue()))
            .andExpect(jsonPath("$.rutFuncionario").value(DEFAULT_RUT_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.nombresFuncionario").value(DEFAULT_NOMBRES_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.apellidopFuncionario").value(DEFAULT_APELLIDOP_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.apellidomFuncionario").value(DEFAULT_APELLIDOM_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.correoFuncionario").value(DEFAULT_CORREO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.fechaContratoFuncionario").value(DEFAULT_FECHA_CONTRATO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.fechaNacimientoFuncionario").value(DEFAULT_FECHA_NACIMIENTO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.imagenFuncionarioContentType").value(DEFAULT_IMAGEN_FUNCIONARIO_CONTENT_TYPE))
            .andExpect(jsonPath("$.imagenFuncionario").value(Base64Utils.encodeToString(DEFAULT_IMAGEN_FUNCIONARIO)))
            .andExpect(jsonPath("$.cargoFuncionario").value(DEFAULT_CARGO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.estamentoFuncionario").value(DEFAULT_ESTAMENTO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.calidadJuridicaFuncionario").value(DEFAULT_CALIDAD_JURIDICA_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.direccionFuncionario").value(DEFAULT_DIRECCION_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.codigoFuncionario").value(DEFAULT_CODIGO_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.gradoFuncionario").value(DEFAULT_GRADO_FUNCIONARIO))
            .andExpect(jsonPath("$.estadoFuncionario").value(DEFAULT_ESTADO_FUNCIONARIO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFuncionario() throws Exception {
        // Get the funcionario
        restFuncionarioMockMvc.perform(get("/api/funcionarios/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFuncionario() throws Exception {
        // Initialize the database
        funcionarioRepository.saveAndFlush(funcionario);
        int databaseSizeBeforeUpdate = funcionarioRepository.findAll().size();

        // Update the funcionario
        Funcionario updatedFuncionario = new Funcionario();
        updatedFuncionario.setId(funcionario.getId());
        updatedFuncionario.setRutFuncionario(UPDATED_RUT_FUNCIONARIO);
        updatedFuncionario.setNombresFuncionario(UPDATED_NOMBRES_FUNCIONARIO);
        updatedFuncionario.setApellidopFuncionario(UPDATED_APELLIDOP_FUNCIONARIO);
        updatedFuncionario.setApellidomFuncionario(UPDATED_APELLIDOM_FUNCIONARIO);
        updatedFuncionario.setCorreoFuncionario(UPDATED_CORREO_FUNCIONARIO);
        updatedFuncionario.setFechaContratoFuncionario(UPDATED_FECHA_CONTRATO_FUNCIONARIO);
        updatedFuncionario.setFechaNacimientoFuncionario(UPDATED_FECHA_NACIMIENTO_FUNCIONARIO);
        updatedFuncionario.setImagenFuncionario(UPDATED_IMAGEN_FUNCIONARIO);
        updatedFuncionario.setImagenFuncionarioContentType(UPDATED_IMAGEN_FUNCIONARIO_CONTENT_TYPE);
        updatedFuncionario.setCargoFuncionario(UPDATED_CARGO_FUNCIONARIO);
        updatedFuncionario.setEstamentoFuncionario(UPDATED_ESTAMENTO_FUNCIONARIO);
        updatedFuncionario.setCalidadJuridicaFuncionario(UPDATED_CALIDAD_JURIDICA_FUNCIONARIO);
        updatedFuncionario.setDireccionFuncionario(UPDATED_DIRECCION_FUNCIONARIO);
        updatedFuncionario.setCodigoFuncionario(UPDATED_CODIGO_FUNCIONARIO);
        updatedFuncionario.setGradoFuncionario(UPDATED_GRADO_FUNCIONARIO);
        updatedFuncionario.setEstadoFuncionario(UPDATED_ESTADO_FUNCIONARIO);

        restFuncionarioMockMvc.perform(put("/api/funcionarios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedFuncionario)))
                .andExpect(status().isOk());

        // Validate the Funcionario in the database
        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeUpdate);
        Funcionario testFuncionario = funcionarios.get(funcionarios.size() - 1);
        assertThat(testFuncionario.getRutFuncionario()).isEqualTo(UPDATED_RUT_FUNCIONARIO);
        assertThat(testFuncionario.getNombresFuncionario()).isEqualTo(UPDATED_NOMBRES_FUNCIONARIO);
        assertThat(testFuncionario.getApellidopFuncionario()).isEqualTo(UPDATED_APELLIDOP_FUNCIONARIO);
        assertThat(testFuncionario.getApellidomFuncionario()).isEqualTo(UPDATED_APELLIDOM_FUNCIONARIO);
        assertThat(testFuncionario.getCorreoFuncionario()).isEqualTo(UPDATED_CORREO_FUNCIONARIO);
        assertThat(testFuncionario.getFechaContratoFuncionario()).isEqualTo(UPDATED_FECHA_CONTRATO_FUNCIONARIO);
        assertThat(testFuncionario.getFechaNacimientoFuncionario()).isEqualTo(UPDATED_FECHA_NACIMIENTO_FUNCIONARIO);
        assertThat(testFuncionario.getImagenFuncionario()).isEqualTo(UPDATED_IMAGEN_FUNCIONARIO);
        assertThat(testFuncionario.getImagenFuncionarioContentType()).isEqualTo(UPDATED_IMAGEN_FUNCIONARIO_CONTENT_TYPE);
        assertThat(testFuncionario.getCargoFuncionario()).isEqualTo(UPDATED_CARGO_FUNCIONARIO);
        assertThat(testFuncionario.getEstamentoFuncionario()).isEqualTo(UPDATED_ESTAMENTO_FUNCIONARIO);
        assertThat(testFuncionario.getCalidadJuridicaFuncionario()).isEqualTo(UPDATED_CALIDAD_JURIDICA_FUNCIONARIO);
        assertThat(testFuncionario.getDireccionFuncionario()).isEqualTo(UPDATED_DIRECCION_FUNCIONARIO);
        assertThat(testFuncionario.getCodigoFuncionario()).isEqualTo(UPDATED_CODIGO_FUNCIONARIO);
        assertThat(testFuncionario.getGradoFuncionario()).isEqualTo(UPDATED_GRADO_FUNCIONARIO);
        assertThat(testFuncionario.getEstadoFuncionario()).isEqualTo(UPDATED_ESTADO_FUNCIONARIO);
    }

    @Test
    @Transactional
    public void deleteFuncionario() throws Exception {
        // Initialize the database
        funcionarioRepository.saveAndFlush(funcionario);
        int databaseSizeBeforeDelete = funcionarioRepository.findAll().size();

        // Get the funcionario
        restFuncionarioMockMvc.perform(delete("/api/funcionarios/{id}", funcionario.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Funcionario> funcionarios = funcionarioRepository.findAll();
        assertThat(funcionarios).hasSize(databaseSizeBeforeDelete - 1);
    }
}
