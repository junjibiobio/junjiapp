package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Telefono;
import com.app.junji.repository.TelefonoRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TelefonoResource REST controller.
 *
 * @see TelefonoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class TelefonoResourceIntTest {

    private static final String DEFAULT_TIPO_TELEFONO = "AAAAA";
    private static final String UPDATED_TIPO_TELEFONO = "BBBBB";
    private static final String DEFAULT_NUMERO_TELEFONO = "AAAAA";
    private static final String UPDATED_NUMERO_TELEFONO = "BBBBB";
    private static final String DEFAULT_ABREVIACION_TELEFONO = "AAAAA";
    private static final String UPDATED_ABREVIACION_TELEFONO = "BBBBB";

    @Inject
    private TelefonoRepository telefonoRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTelefonoMockMvc;

    private Telefono telefono;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TelefonoResource telefonoResource = new TelefonoResource();
        ReflectionTestUtils.setField(telefonoResource, "telefonoRepository", telefonoRepository);
        this.restTelefonoMockMvc = MockMvcBuilders.standaloneSetup(telefonoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        telefono = new Telefono();
        telefono.setTipoTelefono(DEFAULT_TIPO_TELEFONO);
        telefono.setNumeroTelefono(DEFAULT_NUMERO_TELEFONO);
        telefono.setAbreviacionTelefono(DEFAULT_ABREVIACION_TELEFONO);
    }

    @Test
    @Transactional
    public void createTelefono() throws Exception {
        int databaseSizeBeforeCreate = telefonoRepository.findAll().size();

        // Create the Telefono

        restTelefonoMockMvc.perform(post("/api/telefonos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(telefono)))
                .andExpect(status().isCreated());

        // Validate the Telefono in the database
        List<Telefono> telefonos = telefonoRepository.findAll();
        assertThat(telefonos).hasSize(databaseSizeBeforeCreate + 1);
        Telefono testTelefono = telefonos.get(telefonos.size() - 1);
        assertThat(testTelefono.getTipoTelefono()).isEqualTo(DEFAULT_TIPO_TELEFONO);
        assertThat(testTelefono.getNumeroTelefono()).isEqualTo(DEFAULT_NUMERO_TELEFONO);
        assertThat(testTelefono.getAbreviacionTelefono()).isEqualTo(DEFAULT_ABREVIACION_TELEFONO);
    }

    @Test
    @Transactional
    public void checkTipoTelefonoIsRequired() throws Exception {
        int databaseSizeBeforeTest = telefonoRepository.findAll().size();
        // set the field null
        telefono.setTipoTelefono(null);

        // Create the Telefono, which fails.

        restTelefonoMockMvc.perform(post("/api/telefonos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(telefono)))
                .andExpect(status().isBadRequest());

        List<Telefono> telefonos = telefonoRepository.findAll();
        assertThat(telefonos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTelefonos() throws Exception {
        // Initialize the database
        telefonoRepository.saveAndFlush(telefono);

        // Get all the telefonos
        restTelefonoMockMvc.perform(get("/api/telefonos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(telefono.getId().intValue())))
                .andExpect(jsonPath("$.[*].tipoTelefono").value(hasItem(DEFAULT_TIPO_TELEFONO.toString())))
                .andExpect(jsonPath("$.[*].numeroTelefono").value(hasItem(DEFAULT_NUMERO_TELEFONO.toString())))
                .andExpect(jsonPath("$.[*].abreviacionTelefono").value(hasItem(DEFAULT_ABREVIACION_TELEFONO.toString())));
    }

    @Test
    @Transactional
    public void getTelefono() throws Exception {
        // Initialize the database
        telefonoRepository.saveAndFlush(telefono);

        // Get the telefono
        restTelefonoMockMvc.perform(get("/api/telefonos/{id}", telefono.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(telefono.getId().intValue()))
            .andExpect(jsonPath("$.tipoTelefono").value(DEFAULT_TIPO_TELEFONO.toString()))
            .andExpect(jsonPath("$.numeroTelefono").value(DEFAULT_NUMERO_TELEFONO.toString()))
            .andExpect(jsonPath("$.abreviacionTelefono").value(DEFAULT_ABREVIACION_TELEFONO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTelefono() throws Exception {
        // Get the telefono
        restTelefonoMockMvc.perform(get("/api/telefonos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTelefono() throws Exception {
        // Initialize the database
        telefonoRepository.saveAndFlush(telefono);
        int databaseSizeBeforeUpdate = telefonoRepository.findAll().size();

        // Update the telefono
        Telefono updatedTelefono = new Telefono();
        updatedTelefono.setId(telefono.getId());
        updatedTelefono.setTipoTelefono(UPDATED_TIPO_TELEFONO);
        updatedTelefono.setNumeroTelefono(UPDATED_NUMERO_TELEFONO);
        updatedTelefono.setAbreviacionTelefono(UPDATED_ABREVIACION_TELEFONO);

        restTelefonoMockMvc.perform(put("/api/telefonos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTelefono)))
                .andExpect(status().isOk());

        // Validate the Telefono in the database
        List<Telefono> telefonos = telefonoRepository.findAll();
        assertThat(telefonos).hasSize(databaseSizeBeforeUpdate);
        Telefono testTelefono = telefonos.get(telefonos.size() - 1);
        assertThat(testTelefono.getTipoTelefono()).isEqualTo(UPDATED_TIPO_TELEFONO);
        assertThat(testTelefono.getNumeroTelefono()).isEqualTo(UPDATED_NUMERO_TELEFONO);
        assertThat(testTelefono.getAbreviacionTelefono()).isEqualTo(UPDATED_ABREVIACION_TELEFONO);
    }

    @Test
    @Transactional
    public void deleteTelefono() throws Exception {
        // Initialize the database
        telefonoRepository.saveAndFlush(telefono);
        int databaseSizeBeforeDelete = telefonoRepository.findAll().size();

        // Get the telefono
        restTelefonoMockMvc.perform(delete("/api/telefonos/{id}", telefono.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Telefono> telefonos = telefonoRepository.findAll();
        assertThat(telefonos).hasSize(databaseSizeBeforeDelete - 1);
    }
}
