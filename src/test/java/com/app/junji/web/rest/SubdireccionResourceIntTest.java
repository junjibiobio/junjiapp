package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Subdireccion;
import com.app.junji.repository.SubdireccionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SubdireccionResource REST controller.
 *
 * @see SubdireccionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class SubdireccionResourceIntTest {

    private static final String DEFAULT_NOMBRE_SUBDIRECCION = "AAAAA";
    private static final String UPDATED_NOMBRE_SUBDIRECCION = "BBBBB";

    @Inject
    private SubdireccionRepository subdireccionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSubdireccionMockMvc;

    private Subdireccion subdireccion;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SubdireccionResource subdireccionResource = new SubdireccionResource();
        ReflectionTestUtils.setField(subdireccionResource, "subdireccionRepository", subdireccionRepository);
        this.restSubdireccionMockMvc = MockMvcBuilders.standaloneSetup(subdireccionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        subdireccion = new Subdireccion();
        subdireccion.setNombreSubdireccion(DEFAULT_NOMBRE_SUBDIRECCION);
    }

    @Test
    @Transactional
    public void createSubdireccion() throws Exception {
        int databaseSizeBeforeCreate = subdireccionRepository.findAll().size();

        // Create the Subdireccion

        restSubdireccionMockMvc.perform(post("/api/subdireccions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subdireccion)))
                .andExpect(status().isCreated());

        // Validate the Subdireccion in the database
        List<Subdireccion> subdireccions = subdireccionRepository.findAll();
        assertThat(subdireccions).hasSize(databaseSizeBeforeCreate + 1);
        Subdireccion testSubdireccion = subdireccions.get(subdireccions.size() - 1);
        assertThat(testSubdireccion.getNombreSubdireccion()).isEqualTo(DEFAULT_NOMBRE_SUBDIRECCION);
    }

    @Test
    @Transactional
    public void checkNombreSubdireccionIsRequired() throws Exception {
        int databaseSizeBeforeTest = subdireccionRepository.findAll().size();
        // set the field null
        subdireccion.setNombreSubdireccion(null);

        // Create the Subdireccion, which fails.

        restSubdireccionMockMvc.perform(post("/api/subdireccions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subdireccion)))
                .andExpect(status().isBadRequest());

        List<Subdireccion> subdireccions = subdireccionRepository.findAll();
        assertThat(subdireccions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubdireccions() throws Exception {
        // Initialize the database
        subdireccionRepository.saveAndFlush(subdireccion);

        // Get all the subdireccions
        restSubdireccionMockMvc.perform(get("/api/subdireccions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(subdireccion.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombreSubdireccion").value(hasItem(DEFAULT_NOMBRE_SUBDIRECCION.toString())));
    }

    @Test
    @Transactional
    public void getSubdireccion() throws Exception {
        // Initialize the database
        subdireccionRepository.saveAndFlush(subdireccion);

        // Get the subdireccion
        restSubdireccionMockMvc.perform(get("/api/subdireccions/{id}", subdireccion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(subdireccion.getId().intValue()))
            .andExpect(jsonPath("$.nombreSubdireccion").value(DEFAULT_NOMBRE_SUBDIRECCION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubdireccion() throws Exception {
        // Get the subdireccion
        restSubdireccionMockMvc.perform(get("/api/subdireccions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubdireccion() throws Exception {
        // Initialize the database
        subdireccionRepository.saveAndFlush(subdireccion);
        int databaseSizeBeforeUpdate = subdireccionRepository.findAll().size();

        // Update the subdireccion
        Subdireccion updatedSubdireccion = new Subdireccion();
        updatedSubdireccion.setId(subdireccion.getId());
        updatedSubdireccion.setNombreSubdireccion(UPDATED_NOMBRE_SUBDIRECCION);

        restSubdireccionMockMvc.perform(put("/api/subdireccions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSubdireccion)))
                .andExpect(status().isOk());

        // Validate the Subdireccion in the database
        List<Subdireccion> subdireccions = subdireccionRepository.findAll();
        assertThat(subdireccions).hasSize(databaseSizeBeforeUpdate);
        Subdireccion testSubdireccion = subdireccions.get(subdireccions.size() - 1);
        assertThat(testSubdireccion.getNombreSubdireccion()).isEqualTo(UPDATED_NOMBRE_SUBDIRECCION);
    }

    @Test
    @Transactional
    public void deleteSubdireccion() throws Exception {
        // Initialize the database
        subdireccionRepository.saveAndFlush(subdireccion);
        int databaseSizeBeforeDelete = subdireccionRepository.findAll().size();

        // Get the subdireccion
        restSubdireccionMockMvc.perform(delete("/api/subdireccions/{id}", subdireccion.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Subdireccion> subdireccions = subdireccionRepository.findAll();
        assertThat(subdireccions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
