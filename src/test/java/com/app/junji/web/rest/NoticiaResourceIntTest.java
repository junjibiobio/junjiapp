package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Noticia;
import com.app.junji.repository.NoticiaRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the NoticiaResource REST controller.
 *
 * @see NoticiaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class NoticiaResourceIntTest {

    private static final String DEFAULT_TITULO_NOTICIA = "AAAAA";
    private static final String UPDATED_TITULO_NOTICIA = "BBBBB";
    private static final String DEFAULT_ARTICULO_NOTICIA = "AAAAA";
    private static final String UPDATED_ARTICULO_NOTICIA = "BBBBB";
    private static final String DEFAULT_CUERPO_NOTICIA = "AAAAA";
    private static final String UPDATED_CUERPO_NOTICIA = "BBBBB";

    @Inject
    private NoticiaRepository noticiaRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restNoticiaMockMvc;

    private Noticia noticia;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NoticiaResource noticiaResource = new NoticiaResource();
        ReflectionTestUtils.setField(noticiaResource, "noticiaRepository", noticiaRepository);
        this.restNoticiaMockMvc = MockMvcBuilders.standaloneSetup(noticiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        noticia = new Noticia();
        noticia.setTituloNoticia(DEFAULT_TITULO_NOTICIA);
        noticia.setArticuloNoticia(DEFAULT_ARTICULO_NOTICIA);
        noticia.setCuerpoNoticia(DEFAULT_CUERPO_NOTICIA);
    }

    @Test
    @Transactional
    public void createNoticia() throws Exception {
        int databaseSizeBeforeCreate = noticiaRepository.findAll().size();

        // Create the Noticia

        restNoticiaMockMvc.perform(post("/api/noticias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(noticia)))
                .andExpect(status().isCreated());

        // Validate the Noticia in the database
        List<Noticia> noticias = noticiaRepository.findAll();
        assertThat(noticias).hasSize(databaseSizeBeforeCreate + 1);
        Noticia testNoticia = noticias.get(noticias.size() - 1);
        assertThat(testNoticia.getTituloNoticia()).isEqualTo(DEFAULT_TITULO_NOTICIA);
        assertThat(testNoticia.getArticuloNoticia()).isEqualTo(DEFAULT_ARTICULO_NOTICIA);
        assertThat(testNoticia.getCuerpoNoticia()).isEqualTo(DEFAULT_CUERPO_NOTICIA);
    }

    @Test
    @Transactional
    public void getAllNoticias() throws Exception {
        // Initialize the database
        noticiaRepository.saveAndFlush(noticia);

        // Get all the noticias
        restNoticiaMockMvc.perform(get("/api/noticias?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(noticia.getId().intValue())))
                .andExpect(jsonPath("$.[*].tituloNoticia").value(hasItem(DEFAULT_TITULO_NOTICIA.toString())))
                .andExpect(jsonPath("$.[*].articuloNoticia").value(hasItem(DEFAULT_ARTICULO_NOTICIA.toString())))
                .andExpect(jsonPath("$.[*].cuerpoNoticia").value(hasItem(DEFAULT_CUERPO_NOTICIA.toString())));
    }

    @Test
    @Transactional
    public void getNoticia() throws Exception {
        // Initialize the database
        noticiaRepository.saveAndFlush(noticia);

        // Get the noticia
        restNoticiaMockMvc.perform(get("/api/noticias/{id}", noticia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(noticia.getId().intValue()))
            .andExpect(jsonPath("$.tituloNoticia").value(DEFAULT_TITULO_NOTICIA.toString()))
            .andExpect(jsonPath("$.articuloNoticia").value(DEFAULT_ARTICULO_NOTICIA.toString()))
            .andExpect(jsonPath("$.cuerpoNoticia").value(DEFAULT_CUERPO_NOTICIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNoticia() throws Exception {
        // Get the noticia
        restNoticiaMockMvc.perform(get("/api/noticias/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNoticia() throws Exception {
        // Initialize the database
        noticiaRepository.saveAndFlush(noticia);
        int databaseSizeBeforeUpdate = noticiaRepository.findAll().size();

        // Update the noticia
        Noticia updatedNoticia = new Noticia();
        updatedNoticia.setId(noticia.getId());
        updatedNoticia.setTituloNoticia(UPDATED_TITULO_NOTICIA);
        updatedNoticia.setArticuloNoticia(UPDATED_ARTICULO_NOTICIA);
        updatedNoticia.setCuerpoNoticia(UPDATED_CUERPO_NOTICIA);

        restNoticiaMockMvc.perform(put("/api/noticias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNoticia)))
                .andExpect(status().isOk());

        // Validate the Noticia in the database
        List<Noticia> noticias = noticiaRepository.findAll();
        assertThat(noticias).hasSize(databaseSizeBeforeUpdate);
        Noticia testNoticia = noticias.get(noticias.size() - 1);
        assertThat(testNoticia.getTituloNoticia()).isEqualTo(UPDATED_TITULO_NOTICIA);
        assertThat(testNoticia.getArticuloNoticia()).isEqualTo(UPDATED_ARTICULO_NOTICIA);
        assertThat(testNoticia.getCuerpoNoticia()).isEqualTo(UPDATED_CUERPO_NOTICIA);
    }

    @Test
    @Transactional
    public void deleteNoticia() throws Exception {
        // Initialize the database
        noticiaRepository.saveAndFlush(noticia);
        int databaseSizeBeforeDelete = noticiaRepository.findAll().size();

        // Get the noticia
        restNoticiaMockMvc.perform(delete("/api/noticias/{id}", noticia.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Noticia> noticias = noticiaRepository.findAll();
        assertThat(noticias).hasSize(databaseSizeBeforeDelete - 1);
    }
}
