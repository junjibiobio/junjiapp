package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Grupo;
import com.app.junji.repository.GrupoRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GrupoResource REST controller.
 *
 * @see GrupoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class GrupoResourceIntTest {

    private static final String DEFAULT_NOMBRE_GRUPO = "AAAAA";
    private static final String UPDATED_NOMBRE_GRUPO = "BBBBB";

    private static final Double DEFAULT_CAPACIDAD_GRUPO = 1D;
    private static final Double UPDATED_CAPACIDAD_GRUPO = 2D;

    private static final Double DEFAULT_MATRICULA_GRUPO = 1D;
    private static final Double UPDATED_MATRICULA_GRUPO = 2D;

    private static final Double DEFAULT_ASISTENCIA_GRUPO = 1D;
    private static final Double UPDATED_ASISTENCIA_GRUPO = 2D;

    @Inject
    private GrupoRepository grupoRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGrupoMockMvc;

    private Grupo grupo;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GrupoResource grupoResource = new GrupoResource();
        ReflectionTestUtils.setField(grupoResource, "grupoRepository", grupoRepository);
        this.restGrupoMockMvc = MockMvcBuilders.standaloneSetup(grupoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        grupo = new Grupo();
        grupo.setNombreGrupo(DEFAULT_NOMBRE_GRUPO);
        grupo.setCapacidadGrupo(DEFAULT_CAPACIDAD_GRUPO);
        grupo.setMatriculaGrupo(DEFAULT_MATRICULA_GRUPO);
        grupo.setAsistenciaGrupo(DEFAULT_ASISTENCIA_GRUPO);
    }

    @Test
    @Transactional
    public void createGrupo() throws Exception {
        int databaseSizeBeforeCreate = grupoRepository.findAll().size();

        // Create the Grupo

        restGrupoMockMvc.perform(post("/api/grupos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(grupo)))
                .andExpect(status().isCreated());

        // Validate the Grupo in the database
        List<Grupo> grupos = grupoRepository.findAll();
        assertThat(grupos).hasSize(databaseSizeBeforeCreate + 1);
        Grupo testGrupo = grupos.get(grupos.size() - 1);
        assertThat(testGrupo.getNombreGrupo()).isEqualTo(DEFAULT_NOMBRE_GRUPO);
        assertThat(testGrupo.getCapacidadGrupo()).isEqualTo(DEFAULT_CAPACIDAD_GRUPO);
        assertThat(testGrupo.getMatriculaGrupo()).isEqualTo(DEFAULT_MATRICULA_GRUPO);
        assertThat(testGrupo.getAsistenciaGrupo()).isEqualTo(DEFAULT_ASISTENCIA_GRUPO);
    }

    @Test
    @Transactional
    public void checkNombreGrupoIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupoRepository.findAll().size();
        // set the field null
        grupo.setNombreGrupo(null);

        // Create the Grupo, which fails.

        restGrupoMockMvc.perform(post("/api/grupos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(grupo)))
                .andExpect(status().isBadRequest());

        List<Grupo> grupos = grupoRepository.findAll();
        assertThat(grupos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGrupos() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);

        // Get all the grupos
        restGrupoMockMvc.perform(get("/api/grupos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(grupo.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombreGrupo").value(hasItem(DEFAULT_NOMBRE_GRUPO.toString())))
                .andExpect(jsonPath("$.[*].capacidadGrupo").value(hasItem(DEFAULT_CAPACIDAD_GRUPO.doubleValue())))
                .andExpect(jsonPath("$.[*].matriculaGrupo").value(hasItem(DEFAULT_MATRICULA_GRUPO.doubleValue())))
                .andExpect(jsonPath("$.[*].asistenciaGrupo").value(hasItem(DEFAULT_ASISTENCIA_GRUPO.doubleValue())));
    }

    @Test
    @Transactional
    public void getGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);

        // Get the grupo
        restGrupoMockMvc.perform(get("/api/grupos/{id}", grupo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(grupo.getId().intValue()))
            .andExpect(jsonPath("$.nombreGrupo").value(DEFAULT_NOMBRE_GRUPO.toString()))
            .andExpect(jsonPath("$.capacidadGrupo").value(DEFAULT_CAPACIDAD_GRUPO.doubleValue()))
            .andExpect(jsonPath("$.matriculaGrupo").value(DEFAULT_MATRICULA_GRUPO.doubleValue()))
            .andExpect(jsonPath("$.asistenciaGrupo").value(DEFAULT_ASISTENCIA_GRUPO.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGrupo() throws Exception {
        // Get the grupo
        restGrupoMockMvc.perform(get("/api/grupos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);
        int databaseSizeBeforeUpdate = grupoRepository.findAll().size();

        // Update the grupo
        Grupo updatedGrupo = new Grupo();
        updatedGrupo.setId(grupo.getId());
        updatedGrupo.setNombreGrupo(UPDATED_NOMBRE_GRUPO);
        updatedGrupo.setCapacidadGrupo(UPDATED_CAPACIDAD_GRUPO);
        updatedGrupo.setMatriculaGrupo(UPDATED_MATRICULA_GRUPO);
        updatedGrupo.setAsistenciaGrupo(UPDATED_ASISTENCIA_GRUPO);

        restGrupoMockMvc.perform(put("/api/grupos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedGrupo)))
                .andExpect(status().isOk());

        // Validate the Grupo in the database
        List<Grupo> grupos = grupoRepository.findAll();
        assertThat(grupos).hasSize(databaseSizeBeforeUpdate);
        Grupo testGrupo = grupos.get(grupos.size() - 1);
        assertThat(testGrupo.getNombreGrupo()).isEqualTo(UPDATED_NOMBRE_GRUPO);
        assertThat(testGrupo.getCapacidadGrupo()).isEqualTo(UPDATED_CAPACIDAD_GRUPO);
        assertThat(testGrupo.getMatriculaGrupo()).isEqualTo(UPDATED_MATRICULA_GRUPO);
        assertThat(testGrupo.getAsistenciaGrupo()).isEqualTo(UPDATED_ASISTENCIA_GRUPO);
    }

    @Test
    @Transactional
    public void deleteGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);
        int databaseSizeBeforeDelete = grupoRepository.findAll().size();

        // Get the grupo
        restGrupoMockMvc.perform(delete("/api/grupos/{id}", grupo.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Grupo> grupos = grupoRepository.findAll();
        assertThat(grupos).hasSize(databaseSizeBeforeDelete - 1);
    }
}
