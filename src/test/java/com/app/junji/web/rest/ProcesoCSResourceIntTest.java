package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.ProcesoCS;
import com.app.junji.repository.ProcesoCSRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ProcesoCSResource REST controller.
 *
 * @see ProcesoCSResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class ProcesoCSResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_FECHA_PROCESO_CS = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_FECHA_PROCESO_CS = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_FECHA_PROCESO_CS_STR = dateTimeFormatter.format(DEFAULT_FECHA_PROCESO_CS);
    private static final String DEFAULT_TIPO_PROCESO_CS = "AAAAA";
    private static final String UPDATED_TIPO_PROCESO_CS = "BBBBB";
    private static final String DEFAULT_ESTADO_PROCESO_CS = "AAAAA";
    private static final String UPDATED_ESTADO_PROCESO_CS = "BBBBB";

    @Inject
    private ProcesoCSRepository procesoCSRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restProcesoCSMockMvc;

    private ProcesoCS procesoCS;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProcesoCSResource procesoCSResource = new ProcesoCSResource();
        ReflectionTestUtils.setField(procesoCSResource, "procesoCSRepository", procesoCSRepository);
        this.restProcesoCSMockMvc = MockMvcBuilders.standaloneSetup(procesoCSResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        procesoCS = new ProcesoCS();
        procesoCS.setFechaProcesoCS(DEFAULT_FECHA_PROCESO_CS);
        procesoCS.setTipoProcesoCS(DEFAULT_TIPO_PROCESO_CS);
        procesoCS.setEstadoProcesoCS(DEFAULT_ESTADO_PROCESO_CS);
    }

    @Test
    @Transactional
    public void createProcesoCS() throws Exception {
        int databaseSizeBeforeCreate = procesoCSRepository.findAll().size();

        // Create the ProcesoCS

        restProcesoCSMockMvc.perform(post("/api/proceso-cs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(procesoCS)))
                .andExpect(status().isCreated());

        // Validate the ProcesoCS in the database
        List<ProcesoCS> procesoCS = procesoCSRepository.findAll();
        assertThat(procesoCS).hasSize(databaseSizeBeforeCreate + 1);
        ProcesoCS testProcesoCS = procesoCS.get(procesoCS.size() - 1);
        assertThat(testProcesoCS.getFechaProcesoCS()).isEqualTo(DEFAULT_FECHA_PROCESO_CS);
        assertThat(testProcesoCS.getTipoProcesoCS()).isEqualTo(DEFAULT_TIPO_PROCESO_CS);
        assertThat(testProcesoCS.getEstadoProcesoCS()).isEqualTo(DEFAULT_ESTADO_PROCESO_CS);
    }

    @Test
    @Transactional
    public void checkTipoProcesoCSIsRequired() throws Exception {
        int databaseSizeBeforeTest = procesoCSRepository.findAll().size();
        // set the field null
        procesoCS.setTipoProcesoCS(null);

        // Create the ProcesoCS, which fails.

        restProcesoCSMockMvc.perform(post("/api/proceso-cs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(procesoCS)))
                .andExpect(status().isBadRequest());

        List<ProcesoCS> procesoCS = procesoCSRepository.findAll();
        assertThat(procesoCS).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProcesoCS() throws Exception {
        // Initialize the database
        procesoCSRepository.saveAndFlush(procesoCS);

        // Get all the procesoCS
        restProcesoCSMockMvc.perform(get("/api/proceso-cs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(procesoCS.getId().intValue())))
                .andExpect(jsonPath("$.[*].fechaProcesoCS").value(hasItem(DEFAULT_FECHA_PROCESO_CS_STR)))
                .andExpect(jsonPath("$.[*].tipoProcesoCS").value(hasItem(DEFAULT_TIPO_PROCESO_CS.toString())))
                .andExpect(jsonPath("$.[*].estadoProcesoCS").value(hasItem(DEFAULT_ESTADO_PROCESO_CS.toString())));
    }

    @Test
    @Transactional
    public void getProcesoCS() throws Exception {
        // Initialize the database
        procesoCSRepository.saveAndFlush(procesoCS);

        // Get the procesoCS
        restProcesoCSMockMvc.perform(get("/api/proceso-cs/{id}", procesoCS.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(procesoCS.getId().intValue()))
            .andExpect(jsonPath("$.fechaProcesoCS").value(DEFAULT_FECHA_PROCESO_CS_STR))
            .andExpect(jsonPath("$.tipoProcesoCS").value(DEFAULT_TIPO_PROCESO_CS.toString()))
            .andExpect(jsonPath("$.estadoProcesoCS").value(DEFAULT_ESTADO_PROCESO_CS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProcesoCS() throws Exception {
        // Get the procesoCS
        restProcesoCSMockMvc.perform(get("/api/proceso-cs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProcesoCS() throws Exception {
        // Initialize the database
        procesoCSRepository.saveAndFlush(procesoCS);
        int databaseSizeBeforeUpdate = procesoCSRepository.findAll().size();

        // Update the procesoCS
        ProcesoCS updatedProcesoCS = new ProcesoCS();
        updatedProcesoCS.setId(procesoCS.getId());
        updatedProcesoCS.setFechaProcesoCS(UPDATED_FECHA_PROCESO_CS);
        updatedProcesoCS.setTipoProcesoCS(UPDATED_TIPO_PROCESO_CS);
        updatedProcesoCS.setEstadoProcesoCS(UPDATED_ESTADO_PROCESO_CS);

        restProcesoCSMockMvc.perform(put("/api/proceso-cs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedProcesoCS)))
                .andExpect(status().isOk());

        // Validate the ProcesoCS in the database
        List<ProcesoCS> procesoCS = procesoCSRepository.findAll();
        assertThat(procesoCS).hasSize(databaseSizeBeforeUpdate);
        ProcesoCS testProcesoCS = procesoCS.get(procesoCS.size() - 1);
        assertThat(testProcesoCS.getFechaProcesoCS()).isEqualTo(UPDATED_FECHA_PROCESO_CS);
        assertThat(testProcesoCS.getTipoProcesoCS()).isEqualTo(UPDATED_TIPO_PROCESO_CS);
        assertThat(testProcesoCS.getEstadoProcesoCS()).isEqualTo(UPDATED_ESTADO_PROCESO_CS);
    }

    @Test
    @Transactional
    public void deleteProcesoCS() throws Exception {
        // Initialize the database
        procesoCSRepository.saveAndFlush(procesoCS);
        int databaseSizeBeforeDelete = procesoCSRepository.findAll().size();

        // Get the procesoCS
        restProcesoCSMockMvc.perform(delete("/api/proceso-cs/{id}", procesoCS.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProcesoCS> procesoCS = procesoCSRepository.findAll();
        assertThat(procesoCS).hasSize(databaseSizeBeforeDelete - 1);
    }
}
