package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.Jardin;
import com.app.junji.repository.JardinRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the JardinResource REST controller.
 *
 * @see JardinResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class JardinResourceIntTest {


    private static final Double DEFAULT_CODIGO_JARDIN = 1D;
    private static final Double UPDATED_CODIGO_JARDIN = 2D;
    private static final String DEFAULT_NOMBRE_JARDIN = "AAAAA";
    private static final String UPDATED_NOMBRE_JARDIN = "BBBBB";
    private static final String DEFAULT_ESTADO_JARDIN = "AAAAA";
    private static final String UPDATED_ESTADO_JARDIN = "BBBBB";

    private static final LocalDate DEFAULT_FECHA_CREACION_JARDIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_CREACION_JARDIN = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_PROGRAMA_JARDIN = "AAAAA";
    private static final String UPDATED_PROGRAMA_JARDIN = "BBBBB";
    private static final String DEFAULT_MODALIDAD_JARDIN = "AAAAA";
    private static final String UPDATED_MODALIDAD_JARDIN = "BBBBB";
    private static final String DEFAULT_RURALIDAD_JARDIN = "AAAAA";
    private static final String UPDATED_RURALIDAD_JARDIN = "BBBBB";
    private static final String DEFAULT_DIRECCION_JARDIN = "AAAAA";
    private static final String UPDATED_DIRECCION_JARDIN = "BBBBB";
    private static final String DEFAULT_PROVINCIA_JARDIN = "AAAAA";
    private static final String UPDATED_PROVINCIA_JARDIN = "BBBBB";
    private static final String DEFAULT_COMUNA_JARDIN = "AAAAA";
    private static final String UPDATED_COMUNA_JARDIN = "BBBBB";

    private static final Double DEFAULT_LATITUD_JARDIN = 1D;
    private static final Double UPDATED_LATITUD_JARDIN = 2D;

    private static final Double DEFAULT_LONGITUD_JARDIN = 1D;
    private static final Double UPDATED_LONGITUD_JARDIN = 2D;

    @Inject
    private JardinRepository jardinRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restJardinMockMvc;

    private Jardin jardin;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        JardinResource jardinResource = new JardinResource();
        ReflectionTestUtils.setField(jardinResource, "jardinRepository", jardinRepository);
        this.restJardinMockMvc = MockMvcBuilders.standaloneSetup(jardinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        jardin = new Jardin();
        jardin.setCodigoJardin(DEFAULT_CODIGO_JARDIN);
        jardin.setNombreJardin(DEFAULT_NOMBRE_JARDIN);
        jardin.setEstadoJardin(DEFAULT_ESTADO_JARDIN);
        jardin.setFechaCreacionJardin(DEFAULT_FECHA_CREACION_JARDIN);
        jardin.setProgramaJardin(DEFAULT_PROGRAMA_JARDIN);
        jardin.setModalidadJardin(DEFAULT_MODALIDAD_JARDIN);
        jardin.setRuralidadJardin(DEFAULT_RURALIDAD_JARDIN);
        jardin.setDireccionJardin(DEFAULT_DIRECCION_JARDIN);
        jardin.setProvinciaJardin(DEFAULT_PROVINCIA_JARDIN);
        jardin.setComunaJardin(DEFAULT_COMUNA_JARDIN);
        jardin.setLatitudJardin(DEFAULT_LATITUD_JARDIN);
        jardin.setLongitudJardin(DEFAULT_LONGITUD_JARDIN);
    }

    @Test
    @Transactional
    public void createJardin() throws Exception {
        int databaseSizeBeforeCreate = jardinRepository.findAll().size();

        // Create the Jardin

        restJardinMockMvc.perform(post("/api/jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(jardin)))
                .andExpect(status().isCreated());

        // Validate the Jardin in the database
        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeCreate + 1);
        Jardin testJardin = jardins.get(jardins.size() - 1);
        assertThat(testJardin.getCodigoJardin()).isEqualTo(DEFAULT_CODIGO_JARDIN);
        assertThat(testJardin.getNombreJardin()).isEqualTo(DEFAULT_NOMBRE_JARDIN);
        assertThat(testJardin.getEstadoJardin()).isEqualTo(DEFAULT_ESTADO_JARDIN);
        assertThat(testJardin.getFechaCreacionJardin()).isEqualTo(DEFAULT_FECHA_CREACION_JARDIN);
        assertThat(testJardin.getProgramaJardin()).isEqualTo(DEFAULT_PROGRAMA_JARDIN);
        assertThat(testJardin.getModalidadJardin()).isEqualTo(DEFAULT_MODALIDAD_JARDIN);
        assertThat(testJardin.getRuralidadJardin()).isEqualTo(DEFAULT_RURALIDAD_JARDIN);
        assertThat(testJardin.getDireccionJardin()).isEqualTo(DEFAULT_DIRECCION_JARDIN);
        assertThat(testJardin.getProvinciaJardin()).isEqualTo(DEFAULT_PROVINCIA_JARDIN);
        assertThat(testJardin.getComunaJardin()).isEqualTo(DEFAULT_COMUNA_JARDIN);
        assertThat(testJardin.getLatitudJardin()).isEqualTo(DEFAULT_LATITUD_JARDIN);
        assertThat(testJardin.getLongitudJardin()).isEqualTo(DEFAULT_LONGITUD_JARDIN);
    }

    @Test
    @Transactional
    public void checkCodigoJardinIsRequired() throws Exception {
        int databaseSizeBeforeTest = jardinRepository.findAll().size();
        // set the field null
        jardin.setCodigoJardin(null);

        // Create the Jardin, which fails.

        restJardinMockMvc.perform(post("/api/jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(jardin)))
                .andExpect(status().isBadRequest());

        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNombreJardinIsRequired() throws Exception {
        int databaseSizeBeforeTest = jardinRepository.findAll().size();
        // set the field null
        jardin.setNombreJardin(null);

        // Create the Jardin, which fails.

        restJardinMockMvc.perform(post("/api/jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(jardin)))
                .andExpect(status().isBadRequest());

        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoJardinIsRequired() throws Exception {
        int databaseSizeBeforeTest = jardinRepository.findAll().size();
        // set the field null
        jardin.setEstadoJardin(null);

        // Create the Jardin, which fails.

        restJardinMockMvc.perform(post("/api/jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(jardin)))
                .andExpect(status().isBadRequest());

        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJardins() throws Exception {
        // Initialize the database
        jardinRepository.saveAndFlush(jardin);

        // Get all the jardins
        restJardinMockMvc.perform(get("/api/jardins?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(jardin.getId().intValue())))
                .andExpect(jsonPath("$.[*].codigoJardin").value(hasItem(DEFAULT_CODIGO_JARDIN.doubleValue())))
                .andExpect(jsonPath("$.[*].nombreJardin").value(hasItem(DEFAULT_NOMBRE_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].estadoJardin").value(hasItem(DEFAULT_ESTADO_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].fechaCreacionJardin").value(hasItem(DEFAULT_FECHA_CREACION_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].programaJardin").value(hasItem(DEFAULT_PROGRAMA_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].modalidadJardin").value(hasItem(DEFAULT_MODALIDAD_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].ruralidadJardin").value(hasItem(DEFAULT_RURALIDAD_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].direccionJardin").value(hasItem(DEFAULT_DIRECCION_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].provinciaJardin").value(hasItem(DEFAULT_PROVINCIA_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].comunaJardin").value(hasItem(DEFAULT_COMUNA_JARDIN.toString())))
                .andExpect(jsonPath("$.[*].latitudJardin").value(hasItem(DEFAULT_LATITUD_JARDIN.doubleValue())))
                .andExpect(jsonPath("$.[*].longitudJardin").value(hasItem(DEFAULT_LONGITUD_JARDIN.doubleValue())));
    }

    @Test
    @Transactional
    public void getJardin() throws Exception {
        // Initialize the database
        jardinRepository.saveAndFlush(jardin);

        // Get the jardin
        restJardinMockMvc.perform(get("/api/jardins/{id}", jardin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(jardin.getId().intValue()))
            .andExpect(jsonPath("$.codigoJardin").value(DEFAULT_CODIGO_JARDIN.doubleValue()))
            .andExpect(jsonPath("$.nombreJardin").value(DEFAULT_NOMBRE_JARDIN.toString()))
            .andExpect(jsonPath("$.estadoJardin").value(DEFAULT_ESTADO_JARDIN.toString()))
            .andExpect(jsonPath("$.fechaCreacionJardin").value(DEFAULT_FECHA_CREACION_JARDIN.toString()))
            .andExpect(jsonPath("$.programaJardin").value(DEFAULT_PROGRAMA_JARDIN.toString()))
            .andExpect(jsonPath("$.modalidadJardin").value(DEFAULT_MODALIDAD_JARDIN.toString()))
            .andExpect(jsonPath("$.ruralidadJardin").value(DEFAULT_RURALIDAD_JARDIN.toString()))
            .andExpect(jsonPath("$.direccionJardin").value(DEFAULT_DIRECCION_JARDIN.toString()))
            .andExpect(jsonPath("$.provinciaJardin").value(DEFAULT_PROVINCIA_JARDIN.toString()))
            .andExpect(jsonPath("$.comunaJardin").value(DEFAULT_COMUNA_JARDIN.toString()))
            .andExpect(jsonPath("$.latitudJardin").value(DEFAULT_LATITUD_JARDIN.doubleValue()))
            .andExpect(jsonPath("$.longitudJardin").value(DEFAULT_LONGITUD_JARDIN.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJardin() throws Exception {
        // Get the jardin
        restJardinMockMvc.perform(get("/api/jardins/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJardin() throws Exception {
        // Initialize the database
        jardinRepository.saveAndFlush(jardin);
        int databaseSizeBeforeUpdate = jardinRepository.findAll().size();

        // Update the jardin
        Jardin updatedJardin = new Jardin();
        updatedJardin.setId(jardin.getId());
        updatedJardin.setCodigoJardin(UPDATED_CODIGO_JARDIN);
        updatedJardin.setNombreJardin(UPDATED_NOMBRE_JARDIN);
        updatedJardin.setEstadoJardin(UPDATED_ESTADO_JARDIN);
        updatedJardin.setFechaCreacionJardin(UPDATED_FECHA_CREACION_JARDIN);
        updatedJardin.setProgramaJardin(UPDATED_PROGRAMA_JARDIN);
        updatedJardin.setModalidadJardin(UPDATED_MODALIDAD_JARDIN);
        updatedJardin.setRuralidadJardin(UPDATED_RURALIDAD_JARDIN);
        updatedJardin.setDireccionJardin(UPDATED_DIRECCION_JARDIN);
        updatedJardin.setProvinciaJardin(UPDATED_PROVINCIA_JARDIN);
        updatedJardin.setComunaJardin(UPDATED_COMUNA_JARDIN);
        updatedJardin.setLatitudJardin(UPDATED_LATITUD_JARDIN);
        updatedJardin.setLongitudJardin(UPDATED_LONGITUD_JARDIN);

        restJardinMockMvc.perform(put("/api/jardins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedJardin)))
                .andExpect(status().isOk());

        // Validate the Jardin in the database
        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeUpdate);
        Jardin testJardin = jardins.get(jardins.size() - 1);
        assertThat(testJardin.getCodigoJardin()).isEqualTo(UPDATED_CODIGO_JARDIN);
        assertThat(testJardin.getNombreJardin()).isEqualTo(UPDATED_NOMBRE_JARDIN);
        assertThat(testJardin.getEstadoJardin()).isEqualTo(UPDATED_ESTADO_JARDIN);
        assertThat(testJardin.getFechaCreacionJardin()).isEqualTo(UPDATED_FECHA_CREACION_JARDIN);
        assertThat(testJardin.getProgramaJardin()).isEqualTo(UPDATED_PROGRAMA_JARDIN);
        assertThat(testJardin.getModalidadJardin()).isEqualTo(UPDATED_MODALIDAD_JARDIN);
        assertThat(testJardin.getRuralidadJardin()).isEqualTo(UPDATED_RURALIDAD_JARDIN);
        assertThat(testJardin.getDireccionJardin()).isEqualTo(UPDATED_DIRECCION_JARDIN);
        assertThat(testJardin.getProvinciaJardin()).isEqualTo(UPDATED_PROVINCIA_JARDIN);
        assertThat(testJardin.getComunaJardin()).isEqualTo(UPDATED_COMUNA_JARDIN);
        assertThat(testJardin.getLatitudJardin()).isEqualTo(UPDATED_LATITUD_JARDIN);
        assertThat(testJardin.getLongitudJardin()).isEqualTo(UPDATED_LONGITUD_JARDIN);
    }

    @Test
    @Transactional
    public void deleteJardin() throws Exception {
        // Initialize the database
        jardinRepository.saveAndFlush(jardin);
        int databaseSizeBeforeDelete = jardinRepository.findAll().size();

        // Get the jardin
        restJardinMockMvc.perform(delete("/api/jardins/{id}", jardin.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Jardin> jardins = jardinRepository.findAll();
        assertThat(jardins).hasSize(databaseSizeBeforeDelete - 1);
    }
}
