package com.app.junji.web.rest;

import com.app.junji.JunjiwebApp;
import com.app.junji.domain.DireccionRegional;
import com.app.junji.repository.DireccionRegionalRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the DireccionRegionalResource REST controller.
 *
 * @see DireccionRegionalResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JunjiwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class DireccionRegionalResourceIntTest {

    private static final String DEFAULT_NOMBRE_DR = "AAAAA";
    private static final String UPDATED_NOMBRE_DR = "BBBBB";
    private static final String DEFAULT_UBICACION_DR = "AAAAA";
    private static final String UPDATED_UBICACION_DR = "BBBBB";

    private static final Double DEFAULT_LATITUD_DR = 1D;
    private static final Double UPDATED_LATITUD_DR = 2D;

    private static final Double DEFAULT_LONGITUD_DR = 1D;
    private static final Double UPDATED_LONGITUD_DR = 2D;

    @Inject
    private DireccionRegionalRepository direccionRegionalRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDireccionRegionalMockMvc;

    private DireccionRegional direccionRegional;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DireccionRegionalResource direccionRegionalResource = new DireccionRegionalResource();
        ReflectionTestUtils.setField(direccionRegionalResource, "direccionRegionalRepository", direccionRegionalRepository);
        this.restDireccionRegionalMockMvc = MockMvcBuilders.standaloneSetup(direccionRegionalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        direccionRegional = new DireccionRegional();
        direccionRegional.setNombreDR(DEFAULT_NOMBRE_DR);
        direccionRegional.setUbicacionDR(DEFAULT_UBICACION_DR);
        direccionRegional.setLatitudDR(DEFAULT_LATITUD_DR);
        direccionRegional.setLongitudDR(DEFAULT_LONGITUD_DR);
    }

    @Test
    @Transactional
    public void createDireccionRegional() throws Exception {
        int databaseSizeBeforeCreate = direccionRegionalRepository.findAll().size();

        // Create the DireccionRegional

        restDireccionRegionalMockMvc.perform(post("/api/direccion-regionals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(direccionRegional)))
                .andExpect(status().isCreated());

        // Validate the DireccionRegional in the database
        List<DireccionRegional> direccionRegionals = direccionRegionalRepository.findAll();
        assertThat(direccionRegionals).hasSize(databaseSizeBeforeCreate + 1);
        DireccionRegional testDireccionRegional = direccionRegionals.get(direccionRegionals.size() - 1);
        assertThat(testDireccionRegional.getNombreDR()).isEqualTo(DEFAULT_NOMBRE_DR);
        assertThat(testDireccionRegional.getUbicacionDR()).isEqualTo(DEFAULT_UBICACION_DR);
        assertThat(testDireccionRegional.getLatitudDR()).isEqualTo(DEFAULT_LATITUD_DR);
        assertThat(testDireccionRegional.getLongitudDR()).isEqualTo(DEFAULT_LONGITUD_DR);
    }

    @Test
    @Transactional
    public void checkNombreDRIsRequired() throws Exception {
        int databaseSizeBeforeTest = direccionRegionalRepository.findAll().size();
        // set the field null
        direccionRegional.setNombreDR(null);

        // Create the DireccionRegional, which fails.

        restDireccionRegionalMockMvc.perform(post("/api/direccion-regionals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(direccionRegional)))
                .andExpect(status().isBadRequest());

        List<DireccionRegional> direccionRegionals = direccionRegionalRepository.findAll();
        assertThat(direccionRegionals).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDireccionRegionals() throws Exception {
        // Initialize the database
        direccionRegionalRepository.saveAndFlush(direccionRegional);

        // Get all the direccionRegionals
        restDireccionRegionalMockMvc.perform(get("/api/direccion-regionals?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(direccionRegional.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombreDR").value(hasItem(DEFAULT_NOMBRE_DR.toString())))
                .andExpect(jsonPath("$.[*].ubicacionDR").value(hasItem(DEFAULT_UBICACION_DR.toString())))
                .andExpect(jsonPath("$.[*].latitudDR").value(hasItem(DEFAULT_LATITUD_DR.doubleValue())))
                .andExpect(jsonPath("$.[*].longitudDR").value(hasItem(DEFAULT_LONGITUD_DR.doubleValue())));
    }

    @Test
    @Transactional
    public void getDireccionRegional() throws Exception {
        // Initialize the database
        direccionRegionalRepository.saveAndFlush(direccionRegional);

        // Get the direccionRegional
        restDireccionRegionalMockMvc.perform(get("/api/direccion-regionals/{id}", direccionRegional.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(direccionRegional.getId().intValue()))
            .andExpect(jsonPath("$.nombreDR").value(DEFAULT_NOMBRE_DR.toString()))
            .andExpect(jsonPath("$.ubicacionDR").value(DEFAULT_UBICACION_DR.toString()))
            .andExpect(jsonPath("$.latitudDR").value(DEFAULT_LATITUD_DR.doubleValue()))
            .andExpect(jsonPath("$.longitudDR").value(DEFAULT_LONGITUD_DR.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDireccionRegional() throws Exception {
        // Get the direccionRegional
        restDireccionRegionalMockMvc.perform(get("/api/direccion-regionals/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDireccionRegional() throws Exception {
        // Initialize the database
        direccionRegionalRepository.saveAndFlush(direccionRegional);
        int databaseSizeBeforeUpdate = direccionRegionalRepository.findAll().size();

        // Update the direccionRegional
        DireccionRegional updatedDireccionRegional = new DireccionRegional();
        updatedDireccionRegional.setId(direccionRegional.getId());
        updatedDireccionRegional.setNombreDR(UPDATED_NOMBRE_DR);
        updatedDireccionRegional.setUbicacionDR(UPDATED_UBICACION_DR);
        updatedDireccionRegional.setLatitudDR(UPDATED_LATITUD_DR);
        updatedDireccionRegional.setLongitudDR(UPDATED_LONGITUD_DR);

        restDireccionRegionalMockMvc.perform(put("/api/direccion-regionals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedDireccionRegional)))
                .andExpect(status().isOk());

        // Validate the DireccionRegional in the database
        List<DireccionRegional> direccionRegionals = direccionRegionalRepository.findAll();
        assertThat(direccionRegionals).hasSize(databaseSizeBeforeUpdate);
        DireccionRegional testDireccionRegional = direccionRegionals.get(direccionRegionals.size() - 1);
        assertThat(testDireccionRegional.getNombreDR()).isEqualTo(UPDATED_NOMBRE_DR);
        assertThat(testDireccionRegional.getUbicacionDR()).isEqualTo(UPDATED_UBICACION_DR);
        assertThat(testDireccionRegional.getLatitudDR()).isEqualTo(UPDATED_LATITUD_DR);
        assertThat(testDireccionRegional.getLongitudDR()).isEqualTo(UPDATED_LONGITUD_DR);
    }

    @Test
    @Transactional
    public void deleteDireccionRegional() throws Exception {
        // Initialize the database
        direccionRegionalRepository.saveAndFlush(direccionRegional);
        int databaseSizeBeforeDelete = direccionRegionalRepository.findAll().size();

        // Get the direccionRegional
        restDireccionRegionalMockMvc.perform(delete("/api/direccion-regionals/{id}", direccionRegional.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DireccionRegional> direccionRegionals = direccionRegionalRepository.findAll();
        assertThat(direccionRegionals).hasSize(databaseSizeBeforeDelete - 1);
    }
}
